from ._atomicrex import AtomVectorProperty


def _AtomVectorProperty_set_target_values(self, forces):
    self.get_target_values()[:] = forces
# The singular form is intentional in order to maintain
# compatibility with the standard assignment functions.
AtomVectorProperty.target_value = property(AtomVectorProperty.get_target_values,
                                           _AtomVectorProperty_set_target_values)

AtomVectorProperty.computed_value = property(AtomVectorProperty.get_computed_values)
