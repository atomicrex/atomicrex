///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "UserStructure.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"
#include <boost/lexical_cast.hpp>
#include <string.h>

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Constructor.
 ******************************************************************************/
UserStructure::UserStructure(const FPString& id, FitJob* job) : AtomicStructure(id, job) { registerDOF(&_structureCellDOF); }

/******************************************************************************
 * Relaxes the structure's degrees of freedom such that the total energy is
 *minimized.
 ******************************************************************************/
bool UserStructure::relax(bool isFitting)
{
    // Gather degrees of freedom for which relaxation is enabled.
    // Also reset DOFs to initial values if requested.
    vector<DegreeOfFreedom*> completeDOFList;
    listAllDOF(completeDOFList);
    vector<DegreeOfFreedom*> relaxDOFList;
    int numRelaxDOF = 0;
    for(DegreeOfFreedom* dof : completeDOFList) {
        if(dof->relax()) {
            if(dof == &_atomCoordinatesDOF || dof == &_structureCellDOF) {
                relaxDOFList.push_back(dof);
                numRelaxDOF += dof->numScalars();
                if(dof->resetBeforeRelax()) dof->reset();
            }
        }
    }

    if(numRelaxDOF == 0) return true;  // Nothing to relax.

    // Determine if we are already starting with an initally energy of nan,
    // if this is the case abort relaxation and fitting step.
    double energy = computeEnergy(false, isFitting);
    if(!std::isfinite(energy)) {
        MsgLogger(medium) << "Relaxing structure " << id() << " aborted. "
                          << "Energy is " << energy << endl;
        return false;
    }

    MsgLogger(debug) << "Relaxing structure " << id() << " (#dof=" << numRelaxDOF << "):" << endl;

    // Compile list of bound constraints from DOFs.
    vector<Minimizer::BoundConstraints> constraintTypes(numRelaxDOF);
    vector<double> lowerBounds(numRelaxDOF);
    vector<double> upperBounds(numRelaxDOF);
    vector<Minimizer::BoundConstraints>::iterator type_iter = constraintTypes.begin();
    vector<double>::iterator lower_iter = lowerBounds.begin();
    vector<double>::iterator upper_iter = upperBounds.begin();
    for(DegreeOfFreedom* dof : relaxDOFList) dof->getBoundConstraints(type_iter, lower_iter, upper_iter);
    BOOST_ASSERT(type_iter == constraintTypes.end());
    BOOST_ASSERT(lower_iter == lowerBounds.end());
    BOOST_ASSERT(upper_iter == upperBounds.end());

    vector<double> x0(numRelaxDOF);
    double* x0_iter = x0.data();
    for(DegreeOfFreedom* dof : relaxDOFList) dof->exportValue(x0_iter);
    BOOST_ASSERT(x0_iter == x0.data() + x0.size());

    // Define objective function.
    auto function = [this, &relaxDOFList, isFitting](const std::vector<double>& x) -> double {
        const double* src_iter = x.data();
        for(DegreeOfFreedom* dof : relaxDOFList) {
            dof->importValue(src_iter);
        }
        BOOST_ASSERT(src_iter == x.data() + x.size());
        return computeEnergy(false, isFitting);
    };

    // Define gradient function.
    auto gradient = [this, &relaxDOFList, isFitting](const std::vector<double>& x, std::vector<double>& g) -> double {
        // Get atom positions and cell dimensions
        const double* src_iter = x.data();
        for(DegreeOfFreedom* dof : relaxDOFList) {
            dof->importValue(src_iter);
        }
        BOOST_ASSERT(src_iter == x.data() + x.size());
        double e = computeEnergy(true, isFitting, true);

        // Use analytic gradient (forces) computed by the potential for the positions
        auto g_iter = g.begin();

        for(DegreeOfFreedom* dof : relaxDOFList) {
            if(dof == &_atomCoordinatesDOF) {
                for(auto force_iter = atomForces().begin(), stop = --atomForces().end(); force_iter != stop; force_iter++) {
                    // The last atom is kept fixed so we don't need the forces on that atom.
                    *g_iter++ = -force_iter->x();
                    *g_iter++ = -force_iter->y();
                    *g_iter++ = -force_iter->z();
                }
            }
            else if(dof == &_structureCellDOF) {
                _structureCellDOF.getCellForces(virial(), g_iter);
            }
            else {
                throw runtime_error(
                    str(format("DOF '%1%' showed up in relaxation gradient. This should not happen.") % dof->id()));
            }
        }
        // Assertion only valid if we are using a gradient base optimizer
        // BOOST_ASSERT(force_iter == atomForces().end());
        BOOST_ASSERT(g_iter == g.end());
        return e;
    };

    // Initialize minimizer.
    _relaxMinimizer->prepare(std::move(x0), function, gradient);
    _relaxMinimizer->setConstraints(std::move(constraintTypes), std::move(lowerBounds), std::move(upperBounds));

    Minimizer::MinimizerResult result;
    while((result = _relaxMinimizer->iterate()) == Minimizer::MINIMIZATION_CONTINUE) {
        MsgLogger(debug) << "     rlx_iter=" << _relaxMinimizer->itercount() << " E=" << _relaxMinimizer->value()
                         << " fnorm=" << _relaxMinimizer->gradientNorm2() << endl;
    }
    if(result == Minimizer::MINIMIZATION_ERROR)
        throw runtime_error(str(format("The minimizer reported an error when relaxing structure '%1%'.") % id()));

    return true;
}

/// Computes the total energy and optionally the forces of this structure.
double UserStructure::computeEnergy(bool computeForces, bool isFitting, bool suppressRelaxation)
{
    // Make sure that everything is in place (atoms, neighbor lists etc.).
    updateStructure();

    _totalEnergy = 0.0;

    BOOST_ASSERT(_perPotentialData.size() == job()->potentials().size());
    if(computeForces) {
        // Reset forces.
        fill(_atomForcesProperty.values(), Vector3::Zero());

        // Reset virial.
        _virial.fill(0);

        // Calculate energy and forces.
        for(PerPotentialData& perPotential : _perPotentialData) {
            _totalEnergy += perPotential.pot->computeEnergyAndForces(*this, perPotential);
        }

#ifdef _DEBUG
        for(int i = 0; i < numLocalAtoms(); i++) {
            BOOST_ASSERT(std::isnan(atomForces()[i].x()) == false);
            BOOST_ASSERT(std::isnan(atomForces()[i].y()) == false);
            BOOST_ASSERT(std::isnan(atomForces()[i].z()) == false);
        }
#endif

#if 0
            // Compute virial.
            vector<Vector3>::const_iterator f = atomForces().begin();
            vector<Point3>::const_iterator p = atomPositions().begin();
            for(int i = 0; i < numLocalAtoms(); i++, ++f, ++p) {
                _virial[0] += f->x() * p->x();
                _virial[1] += f->y() * p->y();
                _virial[2] += f->z() * p->z();
                _virial[3] += f->z() * p->y();
                _virial[4] += f->z() * p->x();
                _virial[5] += f->y() * p->x();
            }
#endif

        // Compute pressure tensor (in bar units).
        double invVolume = 1.0 / fabs(simulationCell().determinant()) * 1.6021765e6;
        for(int i = 0; i < 6; i++) _pressureTensorProperty[i] = _virial[i] * invVolume;
        _pressureProperty = (_virial[0] + _virial[1] + _virial[2]) * invVolume / 3.0;
    }
    else {
        // Calculate energy only.
        for(PerPotentialData& perPotential : _perPotentialData) {
            _totalEnergy += perPotential.pot->computeEnergy(*this, perPotential);
        }
    }

    MsgLogger(debug) << "Computed energy of structure " << id() << "  E=" << _totalEnergy << "  local atoms: " << numLocalAtoms()
                     << "  total atoms: " << numAtoms() << endl;

    return _totalEnergy;
}

/******************************************************************************
 * Loads a LAMMPS dump file into the user structure.
 ******************************************************************************/
void UserStructure::loadLAMMPSDumpFile(const FPString& filepath)
{
    // Open input file for reading.
    ifstream stream(filepath.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open LAMMPS dump file: %1%") % filepath));

    MsgLogger(debug) << "Parsing LAMMPS dump file:" << endl;

    bool readNumberOfAtoms = false, readBoxBounds = false, readAtoms = false;

    while(!stream.eof()) {
        string line;
        getline(stream, line);

        do {
            if(starts_with(line, "ITEM: TIMESTEP")) {
                getline(stream, line);  // Ignore timestep number
                break;
            }
            else if(starts_with(line, "ITEM: NUMBER OF ATOMS")) {
                // Parse number of atoms.
                int numAtoms;
                stream >> numAtoms;
                if(!stream || numAtoms < 0 || numAtoms > 1e9)
                    throw runtime_error(
                        str(format("LAMMPS dump file parsing error. Invalid number of atoms. File: %1%") % filepath));
                MsgLogger(debug) << "  numatoms " << numAtoms << endl;
                setAtomCount(numAtoms);
                readNumberOfAtoms = true;
                break;
            }
            else if(starts_with(line, "ITEM: BOX BOUNDS")) {
                size_t pbcInfoStart = 16;
                if(starts_with(line, "ITEM: BOX BOUNDS xy xz yz")) pbcInfoStart += 9;

                // Parse PBC flags
                std::array<bool, 3> pbc;
                pbc.fill(true);  // Assume periodic boundary conditions by default.
                istringstream linestream(line.substr(pbcInfoStart));
                for(int k = 0; k < 3; k++) {
                    string pbcflag;
                    linestream >> pbcflag;
                    if(linestream && pbcflag != "pp") pbc[k] = false;
                }
                MsgLogger(debug) << "  pbc " << pbc[0] << " " << pbc[1] << " " << pbc[2] << endl;

                // Parse simulation box dimensions.
                double tiltFactors[3] = {0, 0, 0};
                double simBoxMin[3];
                double simBoxMax[3];
                for(int k = 0; k < 3; k++) {
                    getline(stream, line);
                    istringstream linestream(line);
                    linestream >> simBoxMin[k] >> simBoxMax[k] >> tiltFactors[k];
                }
                if(!stream)
                    throw runtime_error(str(format("LAMMPS dump file parsing error. Invalid box size. File: %1%") % filepath));

                // LAMMPS only stores the outer bounding box of the simulation cell in the dump file.
                // We have to determine the size of the actual triclinic cell.
                simBoxMin[0] -= min(min(min(tiltFactors[0], tiltFactors[1]), tiltFactors[0] + tiltFactors[1]), (FloatType)0.0);
                simBoxMax[0] -= max(max(max(tiltFactors[0], tiltFactors[1]), tiltFactors[0] + tiltFactors[1]), (FloatType)0.0);
                simBoxMin[1] -= min(tiltFactors[2], 0.0);
                simBoxMax[1] -= max(tiltFactors[2], 0.0);
                setupSimulationCell(
                    Matrix3(Vector3(simBoxMax[0] - simBoxMin[0], 0, 0), Vector3(tiltFactors[0], simBoxMax[1] - simBoxMin[1], 0),
                            Vector3(tiltFactors[1], tiltFactors[2], simBoxMax[2] - simBoxMin[2])),
                    Point3(simBoxMin[0], simBoxMin[1], simBoxMin[2]), pbc);
                MsgLogger(debug) << "  cell " << endl << simulationCell();
                readBoxBounds = true;
                break;
            }
            else if(starts_with(line, "ITEM: ATOMS")) {
                // Parse column names.
                int columnPosX = -1;
                int columnPosY = -1;
                int columnPosZ = -1;
                int columnForceX = -1;
                int columnForceY = -1;
                int columnForceZ = -1;
                int columnDisplX = -1;
                int columnDisplY = -1;
                int columnDisplZ = -1;
                int columnId = -1;
                int columnType = -1;
                bool reducedCoordinates = false;
                string subLine = line.substr(11);
                tokenizer<> tokens(subLine);
                int numberOfColumns = 0;
                for(tokenizer<>::iterator token = tokens.begin(); token != tokens.end(); ++token, numberOfColumns++) {
                    string columnName = *token;
                    if(columnName == "x" || columnName == "xu")
                        columnPosX = numberOfColumns;
                    else if(columnName == "y" || columnName == "yu")
                        columnPosY = numberOfColumns;
                    else if(columnName == "z" || columnName == "zu")
                        columnPosZ = numberOfColumns;
                    else if(columnName == "xs") {
                        columnPosX = numberOfColumns;
                        reducedCoordinates = true;
                    }
                    else if(columnName == "ys") {
                        columnPosY = numberOfColumns;
                        reducedCoordinates = true;
                    }
                    else if(columnName == "zs") {
                        columnPosZ = numberOfColumns;
                        reducedCoordinates = true;
                    }
                    else if(columnName == "fx")
                        columnForceX = numberOfColumns;
                    else if(columnName == "fy")
                        columnForceY = numberOfColumns;
                    else if(columnName == "fz")
                        columnForceZ = numberOfColumns;
                    else if(columnName == "dx")
                        columnDisplX = numberOfColumns;
                    else if(columnName == "dy")
                        columnDisplY = numberOfColumns;
                    else if(columnName == "dz")
                        columnDisplZ = numberOfColumns;
                    else if(columnName == "id")
                        columnId = numberOfColumns;
                    else if(columnName == "type")
                        columnType = numberOfColumns;
                }
                if(numberOfColumns == 0)
                    throw runtime_error(str(format("LAMMPS dump file parsing error. Input file does not contain column "
                                                   "identifiers. File format is too old. File: %1%") %
                                            filepath));
                if(columnPosX == -1)
                    throw runtime_error(str(
                        format("LAMMPS dump file parsing error. Input file does not contain X coordinates column. File: %1%") %
                        filepath));
                if(columnPosY == -1)
                    throw runtime_error(str(
                        format("LAMMPS dump file parsing error. Input file does not contain Y coordinates column. File: %1%") %
                        filepath));
                if(columnPosZ == -1)
                    throw runtime_error(str(
                        format("LAMMPS dump file parsing error. Input file does not contain Z coordinates column. File: %1%") %
                        filepath));

                // Parse one atom per line.
                for(int i = 0; i < numLocalAtoms(); i++) {
                    Point3 pos = Point3::Origin();
                    Point3 displ = Point3::Origin();
                    Vector3 force = Vector3::Zero();
                    int id = i + 1;
                    int type = 0;

                    getline(stream, line);
                    istringstream linestream(line);
                    for(int columnIndex = 0; columnIndex < numberOfColumns; columnIndex++) {
                        if(columnPosX == columnIndex)
                            linestream >> pos.x();
                        else if(columnPosY == columnIndex)
                            linestream >> pos.y();
                        else if(columnPosZ == columnIndex)
                            linestream >> pos.z();
                        else if(columnDisplX == columnIndex)
                            linestream >> displ.x();
                        else if(columnDisplY == columnIndex)
                            linestream >> displ.y();
                        else if(columnDisplZ == columnIndex)
                            linestream >> displ.z();
                        else if(columnForceX == columnIndex)
                            linestream >> force.x();
                        else if(columnForceY == columnIndex)
                            linestream >> force.y();
                        else if(columnForceZ == columnIndex)
                            linestream >> force.z();
                        else if(columnIndex == columnId)
                            linestream >> id;
                        else if(columnIndex == columnType) {
                            linestream >> type;
                            type--;
                        }
                        else {
                            string token;
                            linestream >> token;  // Skip column.
                        }
                    }
                    if(!stream || !linestream)
                        throw runtime_error(
                            str(format("LAMMPS dump file parsing error. Invalid atom line. File: %1%") % filepath));

                    // Rescale reduced atom coordinates.
                    if(reducedCoordinates) pos = reducedToAbsolute(pos);

                    // Check atom index.
                    --id;
                    if(id < 0 || id >= numLocalAtoms())
                        throw runtime_error(
                            str(format("LAMMPS dump file parsing error. Atom index out of range. File: %1%") % filepath));

                    // Remap atom type to internal numbering.
                    map<int, int>::const_iterator remap_iter = _atomTypeRemap.find(type);
                    if(remap_iter != _atomTypeRemap.end()) type = remap_iter->second;
                    if(type < 0 || type >= job()->numAtomTypes())
                        throw runtime_error(
                            str(format("LAMMPS dump file parsing error. Atom type out of range. File: %1%") % filepath));

                    atomPositions()[id] = pos;
                    atomDisplacements()[id] = displ;
                    atomTypes()[id] = type;
                    atomForcesProperty().setTargetValue(id, force);

                    MsgLogger(debug) << "  atom: "
                                     << "  id " << id << "  type " << type << "  pos " << pos << "  displ " << displ << "  force "
                                     << force << endl;
                }
                setDirty(ATOM_POSITIONS);
                readAtoms = true;
                return;  // We are done.
            }
            else if(starts_with(line, "ITEM:")) {
                // Skip lines up to next ITEM.
                while(!stream.eof()) {
                    getline(stream, line);
                    if(starts_with(line, "ITEM:")) break;
                }
            }
            else if(line.empty() && stream) {
                // Skip empty lines
                break;
            }
            else {
                throw runtime_error(
                    str(format("LAMMPS dump file parsing error. Invalid line in dump file %1%: %2%") % filepath % line));
            }
        } while(!stream.eof());
    }
    if(!readNumberOfAtoms)
        throw runtime_error(str(format("LAMMPS dump file parsing error. Number of atoms could not be read: %1%") % filepath));
    if(!readBoxBounds)
        throw runtime_error(str(format("LAMMPS dump file parsing error. Box bounds could not be read: %1%") % filepath));
    if(!readAtoms) throw runtime_error(str(format("LAMMPS dump file parsing error. Atoms could not be read: %1%") % filepath));
}

/******************************************************************************
 * Loads a POSCAR formatted file into the user structure. The format is
 * slightly more flexible to allow forces and energies to be provided.
 ******************************************************************************/
void UserStructure::loadPOSCARFile(const FPString& filepath)
{
    // Open input file for reading.
    ifstream stream(filepath.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open POSCAR file: %1%") % filepath));

    FPString line;

    // Skip comment line.
    getline(stream, line);
    MsgLogger(debug) << "Parsing POSCAR file:" << endl;
    MsgLogger(debug) << "  comment " << line << endl;

    // Lattice scaling factor
    getline(stream, line);
    double alat;
    istringstream(line) >> alat;
    MsgLogger(debug) << "  alat " << alat << endl;
    if(alat <= 0) throw runtime_error(str(format("Invalid lattice constant in POSCAR file: %1%") % filepath));

    // Cell metric.
    Matrix3 simCell;
    for(size_t i = 0; i < 3; i++) {
        getline(stream, line);
        istringstream(line) >> simCell(0, i) >> simCell(1, i) >> simCell(2, i);
    }
    simCell = simCell * alat;
    if(simCell.determinant() <= 0) throw runtime_error(str(format("Invalid cell metric in POSCAR file: %1%") % filepath));
    setupSimulationCell(simCell);
    MsgLogger(debug) << "  cell " << endl << simCell;

    // Parse atom type names.
    vector<int> numberOfAtomsPerType;
    vector<FPString> atomNames;
    for(int i = 0; i < 2; i++) {
        getline(stream, line);
        tokenizer<> tokens(line);
        // Try to convert string tokens to integers.
        try {
            numberOfAtomsPerType.clear();
            for(tokenizer<>::iterator str = tokens.begin(); str != tokens.end(); ++str)
                numberOfAtomsPerType.push_back(boost::lexical_cast<int>(*str));
            // boost::transform(tokens, back_inserter(numberOfAtomsPerType), boost::lexical_cast<int,string>);
            break;
        }
        catch(const boost::bad_lexical_cast&) {
            // If the casting to integer fails, then the current line contains the element names.
            atomNames.clear();
            for(tokenizer<>::iterator str = tokens.begin(); str != tokens.end(); ++str) atomNames.push_back(*str);
        }
    }

    if(numberOfAtomsPerType.empty()) throw runtime_error(str(format("Invalid atom type counts in POSCAR file: %1%") % filepath));

    for(std::vector<int>::iterator iter = numberOfAtomsPerType.begin(); iter != numberOfAtomsPerType.end(); ++iter)
        MsgLogger(debug) << "Number of atoms per type " << *iter << endl;

    // Compute cumulative atom type counts.
    vector<int> cumulativeAtomCounts;
    boost::partial_sum(numberOfAtomsPerType, back_inserter(cumulativeAtomCounts));

    setAtomCount(cumulativeAtomCounts.back());

    // Read format of atom coordinates.
    getline(stream, line);
    trim_left(line);
    to_upper(line);
    bool reducedCoordinates;
    if(starts_with(line, "D"))
        reducedCoordinates = true;
    else if(starts_with(line, "C") || starts_with(line, "K"))
        reducedCoordinates = false;
    else
        throw runtime_error(str(format("POSCAR parser can only handle direct coordinates: %1%") % filepath));
    MsgLogger(debug) << "  reducedCoordinates " << reducedCoordinates << endl;

    // Parse atomic coordinates (and optionally force vectors).
    bool containsForceVectors = false;
    for(int i = 0; i < numLocalAtoms(); i++) {
        // Read xyz coordinates.
        Point3 pos;
        getline(stream, line);
        istringstream linestream(line);
        linestream >> pos.x() >> pos.y() >> pos.z();

        // Read force vector components.
        Vector3 force;
        linestream >> force.x() >> force.y() >> force.z();
        if(linestream)
            containsForceVectors = true;
        else if(containsForceVectors)
            throw runtime_error(str(format("POSCAR contains invalid force vector for atom %1%: %2%") % (i + 1) % filepath));

        int atomType = upper_bound(cumulativeAtomCounts, i) - cumulativeAtomCounts.begin();
        if(_atomTypeRemap.empty() && !atomNames.empty()) {
            // No remap command given. Now the atom types are set according to the
            // atom names read in earlier.
            int sum = 0;
            for(int j = 0; j < numberOfAtomsPerType.size(); j++) {
                sum += numberOfAtomsPerType[j];
                if(sum > i) {
                    atomType = job()->atomTypeIndex(atomNames[j]);
                    break;
                }
            }
        }
        else {
            // Remap atom type to internal numbering.
            map<int, int>::const_iterator remap_iter = _atomTypeRemap.find(atomType);
            if(remap_iter != _atomTypeRemap.end()) atomType = remap_iter->second;
            if(atomType < 0 || atomType >= job()->numAtomTypes())
                throw runtime_error(
                    str(format("POSCAR file parsing error. Atom type is out of range for atom %1%: %2%") % (i + 1) % filepath));
        }

        // Rescale reduced atom coordinates.
        if(reducedCoordinates) pos = reducedToAbsolute(pos);

        // Store values in appropriate arrays.
        atomPositions()[i] = pos;
        atomTypes()[i] = atomType;
        atomForcesProperty().setTargetValue(i, force);
    }

    MsgLogger(debug) << "  containsForceVectors " << containsForceVectors << endl;

    setDirty(ATOM_POSITIONS);
}

/******************************************************************************
 * Parses the structure-specific parameters in the XML element in the job file.
 ******************************************************************************/
void UserStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse <remap-atomtype> elements.
    for(XML::Element remapElement = structureElement.firstChildElement("remap-atomtype"); remapElement;
        remapElement = remapElement.nextSibling("remap-atomtype")) {
        int fromType = remapElement.parseIntParameterAttribute("from");
        int toType = job()->parseAtomTypeAttribute(remapElement, "to");
        _atomTypeRemap[fromType] = toType;
    }

    // Parse structure definition, which can either be a direct
    // specification of the cell geometry and atom coordinates using the <cell> element, or
    // a reference to an external file.
    XML::Element cellElement = structureElement.firstChildElement("cell");
    if(cellElement) {
        parseCellDefinition(cellElement);
    }
    else {
        XML::Element inputElement = structureElement.firstChildElement("lammps-dump");
        if(inputElement) {
            loadLAMMPSDumpFile(structureElement.parsePathParameterElement("lammps-dump"));
        }
        else {
            inputElement = structureElement.firstChildElement("poscar-file");
            if(inputElement)
                loadPOSCARFile(structureElement.parsePathParameterElement("poscar-file"));
            else
                throw runtime_error(str(format("Element <user-structure> at line %1% of XML file must contain a valid input file "
                                               "element or a <cell> element.") %
                                        structureElement.lineNumber()));
        }
    }

    // Parse <pbc> element.
    XML::Element pbcElement = structureElement.firstChildElement("pbc");
    std::array<bool, 3> pbc = this->pbc();
    if(pbcElement) {
        pbc[0] = pbcElement.parseOptionalBooleanParameterAttribute("x", pbc[0]);
        pbc[1] = pbcElement.parseOptionalBooleanParameterAttribute("y", pbc[1]);
        pbc[2] = pbcElement.parseOptionalBooleanParameterAttribute("z", pbc[2]);
    }
    setupSimulationCell(simulationCell(), simulationCellOrigin(), pbc);

    XML::Element forcesElement = structureElement.firstChildElement("atomic-forces");
    if(forcesElement) {
        parseTargetForces(forcesElement);
    }

    // Parse <deformation> element.
    FPString deformationId = structureElement.parseOptionalStringParameterElement("deformation");
    if(!deformationId.empty()) {
        const Matrix3* deformation = job()->lookupDeformation(deformationId);
        if(!deformation)
            throw runtime_error(str(format("Reference to undefined deformation '%1%' in line %2% of job file.") % deformationId %
                                    structureElement.expectChildElement("deformation").lineNumber()));
        deformSimulationCell(*deformation);
    }
}

/******************************************************************************
 * Parses the <cell> element form the input job file.
 ******************************************************************************/
void UserStructure::parseCellDefinition(XML::Element cellElement)
{
    // Parse <a1>, <a2>, <a3> elements containing cell geometry.
    Matrix3 cellMatrix;
    const char* vectorTagNames[3] = {"a1", "a2", "a3"};
    for(int index = 0; index < 3; index++) {
        XML::Element aElement = cellElement.expectChildElement(vectorTagNames[index]);
        cellMatrix.column(index).x() = aElement.parseFloatParameterAttribute("x");
        cellMatrix.column(index).y() = aElement.parseFloatParameterAttribute("y");
        cellMatrix.column(index).z() = aElement.parseFloatParameterAttribute("z");
    }
    setupSimulationCell(cellMatrix);

    XML::Element atomsElement = cellElement.expectChildElement("atoms");

    // Count <atom> elements.
    int natoms = 0;
    for(XML::Element atomElement = atomsElement.firstChildElement(); atomElement; atomElement = atomElement.nextSibling()) {
        natoms++;
    }
    if(natoms == 0)
        throw runtime_error(str(format("Element <atoms> at line %1% of XML file must contain at least one <atom> element.") %
                                atomsElement.lineNumber()));
    setAtomCount(natoms);

    // Parse <atom> elements.
    int atomindex = 0;
    for(XML::Element atomElement = atomsElement.firstChildElement(); atomElement; atomElement = atomElement.nextSibling()) {
        atomElement.expectTag("atom");

        Point3 pos;
        pos.x() = atomElement.parseFloatParameterAttribute("x");
        pos.y() = atomElement.parseFloatParameterAttribute("y");
        pos.z() = atomElement.parseFloatParameterAttribute("z");
        int type = job()->parseAtomTypeAttribute(atomElement, "type");

        // Remap atom type to internal numbering.
        map<int, int>::const_iterator remap_iter = _atomTypeRemap.find(type);
        if(remap_iter != _atomTypeRemap.end()) type = remap_iter->second;
        if(type < 0 || type >= job()->numAtomTypes())
            throw runtime_error(
                str(format("Atom type out of range in <atom> element at line %1% of XML file.") % atomElement.lineNumber()));

        // Rescale reduced atom coordinates.
        if(atomElement.parseOptionalBooleanParameterAttribute("reduced", false)) pos = reducedToAbsolute(pos);

        atomPositions()[atomindex] = pos;
        atomTypes()[atomindex] = type;

        atomindex++;
    }
}

void UserStructure::parseTargetForces(XML::Element forcesElement)
{
    for(XML::Element forceElement = forcesElement.firstChildElement(); forceElement; forceElement = forceElement.nextSibling()) {
        forceElement.expectTag("force");
        Vector3 force;
        force[0] = forceElement.parseFloatParameterAttribute("x");
        force[1] = forceElement.parseFloatParameterAttribute("y");
        force[2] = forceElement.parseFloatParameterAttribute("z");
        int atomindex = forceElement.parseIntParameterAttribute("i");
        atomForcesProperty().setTargetValue(atomindex, force);
    }
}

}
