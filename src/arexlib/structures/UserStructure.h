///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AtomicStructure.h"
#include "../dof/StructureCellDOF.h"
#include "../properties/FitProperty.h"

namespace atomicrex {

/**
 * Class for user-defined structures that are read in from an external file.
 */
class UserStructure : public AtomicStructure
{
public:
    /// Constructor.
    ///
    /// \param id ID of the structure.
    /// \param job Pointer to the Fitjob.
    UserStructure(const FPString& id, FitJob* job);

    /// Relaxes the structural degrees of freedom.
    bool relax(bool isFitting) override;

    /// Computes the total energy and optionally the forces of this structure.
    /// isFitting and suppressRelaxation only for override...
    ///
    /// \param computeForces Flag that determines if forces are calculated.
    /// \param isFitting Flag is not used here but we need to override computeEnergy of AtomicStructure
    /// \param supressRelaxation Flag is not used here but we need to override computeEnergy of AtomicStructure
    double computeEnergy(bool computeForces, bool isFitting, bool suppressRelaxation = false) override;

    /// Loads a LAMMPS dump file into the user structure.
    void loadLAMMPSDumpFile(const FPString& filepath);

    /// Loads a POSCAR file into the user structure.
    void loadPOSCARFile(const FPString& filepath);

public:
    /// Parses the structure-specific parameters from the XML element in the job file.
    virtual void parse(XML::Element structureElement) override;

protected:
    /// Parses the <cell> element form the input job file.
    void parseCellDefinition(XML::Element cellElement);
    void parseTargetForces(XML::Element forcesElement);

private:
    /// This map translates atom types in the input file to our own internal atom types.
    std::map<int, int> _atomTypeRemap;

    /// This DOF encapsulates the degrees of freedom of the cell.
    StructureCellDOF _structureCellDOF;
};

}  // End of namespace
