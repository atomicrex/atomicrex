///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../job/FitGroup.h"
#include "../job/FitJob.h"
#include "Minimizer.h"

namespace atomicrex {

/**
 * Does Split Bregman iterations.
 */
class SplitBregmanMinimizer : public Minimizer
{
public:
    /// Constructor.
    ///
    /// \param job Pointer to the Fitjob
    /// \param intervalForPrinting Determines the frequency for printing the DOF list.
    /// \param isFitMinimizer Flag that saves if the minimizer is used for structure optimization or
    ///                       parameter optimization.
    SplitBregmanMinimizer(FitJob* job, int intervalForPrinting = 0, bool isFitMinimizer = false)
        : Minimizer(job, intervalForPrinting, "SplitBregman", isFitMinimizer)
    {
    }

    /// Constructor that copies another minimizer.
    ///
    /// \param other LBFGS minimizer that is copied.
    SplitBregmanMinimizer(const SplitBregmanMinimizer& other);

    /// Initializes the minimizer by setting the objective function and the starting vector.
    /// Must be called once before entering the minimization loop.
    ///
    /// \param x0 An R-value reference to the starting vector.
    ///           The length of this vector determines the number of dimensions.
    ///           The function transfers the vector to internal storage. That means the
    ///           passed-in vector will no longer be valid after the function returns.
    /// \param func The object that computes the value of the objective function at a given point x.
    /// \param gradient An optional function object that computes the (analytic) gradient of the objective function
    ///                 at a given point x (and also the value of the objective function).
    ///                 If no gradient function is provided, and the minimization algorithm requires
    ///                 the gradient, the minimizer will compute it using finite differences by
    ///                 evaluating \a func several times.
    virtual void prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                         const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient =
                             std::function<double(const std::vector<double>&, std::vector<double>&)>()) override;

    /// Sets constraints for the variation of the parameters.
    /// Must be called after prepare() and before the minimization loop is entered.
    /// All arguments passed to this function will be transferred into internal storage of this class.
    /// The input vectors become invalid when the function returns.
    virtual void setConstraints(std::vector<BoundConstraints>&& constraintTypes, std::vector<double>&& lowerBounds,
                                std::vector<double>&& upperBounds) override;

    /// Performs one Split Bregman iteration.
    virtual MinimizerResult iterate() override;

    /// Returns the last diff.
    double diff() const { return _diff; }

    /// Returns the last norm of x.
    double xNorm() const { return _l1norm; }

    /// Parses the minimizer's parameters from the XML file.
    virtual void parse(XML::Element minimizerElement) override;

    /// Function that creates a copy of this minimizer.
    virtual std::unique_ptr<Minimizer> clone() override;

private:
    /// Do shrink operation on _d, using _b, _lambda, _mu and the x supplied.
    void shrinkAll(const std::vector<double>& x);

    /// Do the arithmetic operations on _d and _b, using _mu and the x supplied.
    void doArithmetic(const std::vector<double>& x);

    /// Compute l1 or l2 norm of x1-x2.
    double computeDiff(const std::vector<double>& x1, const std::vector<double>& x2, int whichNorm);

    /// Compute l1 or l2 norm of x.
    double computeNorm(const std::vector<double>& x, int whichNorm);

private:
    std::vector<double> _d;
    std::vector<double> _b;

    /// The secondary minimizer used by Split Bregman.
    std::unique_ptr<Minimizer> _submin;

    // Split Bregman control parameters:
    double _mu = 0.01;
    double _lambda = 100.0;

    double _diff;
    double _l1norm;
};

}  // End of namespace
