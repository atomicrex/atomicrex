///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DerivedProperty.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Constructor.
 ******************************************************************************/
DerivedProperty::DerivedProperty(FPString id, FPString unit, FitJob* job) : ScalarFitProperty(id, unit, job)
{
    // Activate output by default.
    setOutputEnabled(true);

    // Register this derived property with the job.
    job->addDerivedProperty(this);
}

/******************************************************************************
 * Parse the contents of the <property> element in the job file.
 ******************************************************************************/
void DerivedProperty::parse(XML::Element propertyElement)
{
    ScalarFitProperty::parse(propertyElement);

    // Get Equation from xml.
    FPString equation = propertyElement.parseStringParameterAttribute("equation");

    // Boundaries of the variables.
    auto start = equation.find('[');
    auto end = equation.find(']');
    int n = 0;
    // Go through the equation and deal with all variables
    while(start != FPString::npos) {
        // get the propertyName and the corresponding variable name
        FPString propertyName = equation.substr(start + 1, end - start - 1);
        FPString variableName = str(format("v_%1%") % n++);

        // replace the variables in the equation string.
        equation.replace(start, end - start + 1, variableName);

        // get the corresponding property and enable it for fitting or output
        ScalarFitProperty* property = job()->getScalarFitProperty(propertyName);
        if(!property->fitEnabled()) {
            property->setOutputEnabled(true);
            property->setRelativeWeight(0);
            if(fitEnabled()) {
                property->setFitEnabled(true);
            }
        }
        _inputProperties.push_back(property);

        // get boundaries of the next variable.
        start = equation.find('[');
        end = equation.find(']');
    }

    // Allocate storage for variables and register them with the parser.
    _values.resize(_inputProperties.size());
    for(size_t i = 0; i < _values.size(); i++) {
        _values[i] = *_inputProperties[i];
        _parser.DefineVar(str(format("v_%1%") % i), &_values[i]);
    }

    // Feed expression to the parser.
    _parser.SetExpr(equation);
}

/******************************************************************************
 * Lets the property object compute the current value of the property.
 ******************************************************************************/
void DerivedProperty::compute()
{
    // Update values
    for(size_t i = 0; i < _values.size(); i++) {
        _values[i] = *_inputProperties[i];
    }
    // Evaluate expression
    _computedValue = _parser.Eval();
    MsgLogger(debug) << "Computed energy of derived property " << id() << " Value=" << _computedValue << endl;
}
}
