///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FitJob.h"
#include "../potentials/TabulatedEAMPotential.h"
#include "../potentials/TabulatedMEAMPotential.h"
#include "../potentials/LennardJonesPotential.h"
#include "../potentials/CDIPotential.h"
#include "../potentials/PairPotential.h"
#include "../potentials/TersoffPotential.h"
#include "../potentials/ABOPotential.h"
#include "../potentials/EAMPotential.h"
#include "../potentials/MEAMPotential.h"
#include "../potentials/AngularDependentPotential.h"
#include "../potentials/ParameterConstraint.h"
#include "../properties/FitProperty.h"
#include "../structures/AtomicStructure.h"
#include "../minimizers/Minimizer.h"
#include "../minimizers/LBFGSMinimizer.h"
#include "../util/xml/XMLUtilities.h"
#include "../resources/JobFileSchema.h"

namespace atomicrex {

// From Logger.h:
LoggerVerbosity MsgLogger::_globalLoggerVerbosity = medium;
int _MsgLoggerPrecision;

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor
******************************************************************************/
FitJob::FitJob() : _rootGroup("ROOT", this) {}

/******************************************************************************
* Destructor
******************************************************************************/
FitJob::~FitJob()
{
    for(Potential* p : _potentials) delete p;
    if (_outfile.is_open()) {
          std::cout.rdbuf(_stream_buffer_cout);
          _outfile.close();
       }
    for(ParameterConstraint* c: _parameterConstraintList) delete c;
}

/******************************************************************************
* Assigns absolute weights to the structural and the potential fit properties.
******************************************************************************/
void FitJob::assignAbsoluteWeights()
{
    // Assign absolute weights to the group(s) and by extension
    // the structures and structural properties.
    if(_rootGroup.fitEnabled())
        _rootGroup.assignAbsoluteWeights(1);
    else
        // This will disable all structural properties.
        _rootGroup.assignAbsoluteWeights(0);

    // Assign weights to the potential properties. For them the
    // absolute weight is simply equal to their relative weights.
    for(Potential* pot : potentials()) {
        for(FitProperty* prop : pot->properties()) {
            prop->assignAbsoluteWeights(prop->relativeWeight());
        }
    }
}

/******************************************************************************
* Parses the job description from the XML file.
******************************************************************************/
void FitJob::parse(const FPString& file)
{
    // This initialize the XML parser library and checks for potential ABI mismatches
    // between the version the program was compiled for and the actual shared library used.
    XML::Context context;

    // Load XML file into memory.
    auto document = XML::make_scoped(xmlParseFile(file.c_str()), xmlFreeDoc);
    if(!document) throw runtime_error(str(format("Failed to load XML file '%1%'") % file));

    // Handle XInclude directives.
    if(xmlXIncludeProcess(document.get()) < 0)
        throw runtime_error(str(format("Failed to process XInclude directives in XML file '%1%'") % file));

    // Check root element.
    XML::Element rootElement(xmlDocGetRootElement(document.get()));
    if(!rootElement || !rootElement.tagEquals("job")) throw runtime_error("Expected <job> XML root element.");

    // Create schema parser context, which gets passed the raw schema data.
    auto schemaParserContext = XML::make_scoped(
        xmlSchemaNewMemParserCtxt(reinterpret_cast<const char*>(resources_JobFile_xsd), resources_JobFile_xsd_len),
        xmlSchemaFreeParserCtxt);
    if(!schemaParserContext) throw runtime_error("Failed to initialize XSD schema to validate input XML file.");

    // Parse XSD schema.
    auto schema = XML::make_scoped(xmlSchemaParse(schemaParserContext.get()), xmlSchemaFree);
    if(!schema) throw runtime_error("Failed to initialize XSD schema to validate input XML file.");

    // Set up schema validation context.
    auto validationContext = XML::make_scoped(xmlSchemaNewValidCtxt(schema.get()), xmlSchemaFreeValidCtxt);
    xmlSchemaSetValidErrors(validationContext.get(), (xmlSchemaValidityErrorFunc)fprintf, (xmlSchemaValidityWarningFunc)fprintf,
                            stderr);

    // Validate XML document against schema.
    if(xmlSchemaValidateDoc(validationContext.get(), document.get()) != 0) {
        throw runtime_error(str(format("Input file '%1%' is not valid.") % file));
    }

    // Parse if output should be written to a file.
    FPString outputString = rootElement.parseOptionalStringParameterElement("output-file");
    if(!outputString.empty()) {
        setOutputFile(outputString);
    }

    // Parse verbosity level.
    FPString verbosityString = rootElement.parseOptionalStringParameterElement("verbosity");
    if(!verbosityString.empty()) {
        if(verbosityString == "none")
            MsgLogger::setVerbosity(none);
        else if(verbosityString == "minimum")
            MsgLogger::setVerbosity(minimum);
        else if(verbosityString == "medium")
            MsgLogger::setVerbosity(medium);
        else if(verbosityString == "maximum")
            MsgLogger::setVerbosity(maximum);
        else if(verbosityString == "debug")
            MsgLogger::setVerbosity(debug);
        else
            throw runtime_error(str(format("Invalid verbosity level in line %1% of XML file: %2%") %
                                    rootElement.expectChildElement("verbosity").lineNumber() % verbosityString));
    }

    // Parse job name.
    MsgLogger(debug) << "Parse job name." << endl;
    _name = rootElement.parseStringParameterElement("name");

    // Parse real number precision output level.
    MsgLogger(debug) << "Parse real number precision output level." << endl;
    int realPrecision = rootElement.parseOptionalIntParameterElement("real-precision", 6);
    if (realPrecision <1 | realPrecision > 16){
        throw runtime_error(
            "Real precision value should be between 1 and 16"
        );
    }
    _MsgLoggerPrecision = realPrecision;

    // Parse tolerance presets.
    FPString tolerancePresetString = rootElement.parseOptionalStringParameterElement("tolerance-preset");
    if(!tolerancePresetString.empty()) {
        if(tolerancePresetString == "uniform")
            _tolerancePreset = Uniform;
        else if(tolerancePresetString == "balanced")
            _tolerancePreset = Balanced;
        else if(tolerancePresetString == "accurate-energies")
            _tolerancePreset = AccurateEnergies;
        else
            throw runtime_error(str(format("Invalid tolerance preset in line %1% of XML file: %2%") %
                                    rootElement.expectChildElement("tolerance-preset").lineNumber() % tolerancePresetString));
    }

    // Parse atom types definition.
    MsgLogger(debug) << "Parse atom types definition." << endl;
    parseAtomTypesElement(rootElement.expectChildElement("atom-types"));

    // Parse potentials.
    MsgLogger(debug) << "Parse potentials." << endl;
    parsePotentialsElement(rootElement.expectChildElement("potentials"));

    // Parse <relaxation> element
    MsgLogger(debug) << "Parse <relaxation> element." << endl;
    XML::Element relaxationElement = rootElement.firstChildElement("relaxation");
    if(relaxationElement) {
        // Parse minimizer and its parameters.
        XML::Element minimizerElement = relaxationElement.firstChildElement();
        if(!minimizerElement)
            throw runtime_error(str(format("Expected minimizer element in <%1%> element at line %2% of XML file.") %
                                    relaxationElement.tag() % relaxationElement.lineNumber()));
        _relaxMinimizer = Minimizer::createAndParse(minimizerElement, this);
    }
    else {
        MsgLogger(debug) << "Using LBFGS Minimizer as default minimizer." << endl;
        _relaxMinimizer = std::unique_ptr<LBFGSMinimizer>(new LBFGSMinimizer(this));
    }

    // Parse deformations.
    MsgLogger(debug) << "Parse deformations." << endl;
    XML::Element deformationsElement = rootElement.firstChildElement("deformations");
    if(deformationsElement) parseDeformationsElement(deformationsElement);

    // Parse structures and derived properties.
    MsgLogger(debug) << "Parse structures and derived properties." << endl;
    _rootGroup.parseElement(rootElement.expectChildElement("structures"), this);

    // Parse <fitting> element.
    MsgLogger(debug) << "Parse <fitting> element." << endl;
    XML::Element fittingElement = rootElement.firstChildElement("fitting");
    if(fittingElement) parseFittingElement(fittingElement);

    // Parse <validate-potentials> element.
    MsgLogger(debug) << "Parse <validate-potentials> element." << endl;
    _potentialValidationEnabled = (bool)rootElement.firstChildElement("validate-potentials");

    MsgLogger(debug) << "Parse <parameter-constraints> element." << endl;
    XML::Element constraintElement = rootElement.firstChildElement("parameter-constraints");
    if(constraintElement) parseParameterConstraintsElement(constraintElement); 
}

void FitJob::setOutputFile(FPString& filename) {
    _outfile.open(filename);
    std::cout.rdbuf(_outfile.rdbuf());
}

/******************************************************************************
* Parses the contents of the <atom-types> element in the job file.
******************************************************************************/
void FitJob::parseAtomTypesElement(XML::Element typesElement)
{
    // Parse list of species.
    for(XML::Element speciesElement = typesElement.firstChildElement(); speciesElement;
        speciesElement = speciesElement.nextSibling()) {
        speciesElement.expectTag("species");

        FPString speciesName = speciesElement.textContent();
        boost::trim(speciesName);
        double mass = speciesElement.parseOptionalFloatParameterAttribute("mass", 0.0);
        int atomicNumber = speciesElement.parseOptionalIntParameterAttribute("atomic-number", 0);
        if(!addAtomType(speciesName, mass, atomicNumber))
            throw runtime_error(
                str(format("Invalid atomic species name in line %1% of XML file.") % speciesElement.lineNumber()));
    }

    if(_natomtypes == 0)
        throw runtime_error(
            str(format("At least one atomic species must be defined in line %1% of XML file.") % typesElement.lineNumber()));
}

/******************************************************************************
* Parses the contents of the <potentials> element in the job file.
******************************************************************************/
void FitJob::parsePotentialsElement(XML::Element potentialsElement)
{
    // Parse list of potentials.
    for(XML::Element potentialElement = potentialsElement.firstChildElement(); potentialElement;
        potentialElement = potentialElement.nextSibling()) {
        Potential* pot;
        FPString id = potentialElement.parseOptionalStringParameterAttribute("id");
        if(potentialElement.tagEquals("eam"))
            pot = new EAMPotential(id, this);
        else if(potentialElement.tagEquals("meam"))
            pot = new MEAMPotential(id, this);
        else if(potentialElement.tagEquals("adp"))
            pot = new ADPotential(id, this);
        else if(potentialElement.tagEquals("table-eam"))
            pot = new TabulatedEAMPotential(id, this);
        else if(potentialElement.tagEquals("table-meam"))
            pot = new TabulatedMEAMPotential(id, this);
        else if(potentialElement.tagEquals("cdip"))
            pot = new CDIPotential(id, this);
        else if(potentialElement.tagEquals("pair-potential"))
            pot = new PairPotential(id, this);
        else if(potentialElement.tagEquals("lennard-jones"))
            pot = new LennardJonesPotential(id, this);
        else if(potentialElement.tagEquals("tersoff"))
            pot = new TersoffPotential(id, this);
        else if(potentialElement.tagEquals("abop"))
            pot = new ABOPotential(id, this);
        else
            throw runtime_error(str(format("Unknown potential type <%1%> in line %2% of job file.") % potentialElement.tag() %
                                    potentialElement.lineNumber()));
        _potentials.push_back(pot);

        // Insert potential into interaction table.
        vector<int> speciesAList = parseAtomTypesAttribute(potentialElement, "species-a");
        vector<int> speciesBList = parseAtomTypesAttribute(potentialElement, "species-b");
        for(int a : speciesAList) {
            for(int b : speciesBList) {
                pot->enableInteraction(a, b);
            }
        }

        // Parse potential-specific part.
        pot->parse(potentialElement);

        // Determine maximum cutoff radius.
        _maximumCutoff = max(_maximumCutoff, pot->cutoff());
    }
}

/******************************************************************************
* Parses the contents of the <fitting> element in the job file.
******************************************************************************/
void FitJob::parseFittingElement(XML::Element fittingElement)
{
    if(!fittingElement.parseOptionalBooleanParameterAttribute("enabled", true)) return;

    int intervalForPrinting = fittingElement.parseOptionalIntParameterAttribute("output-interval", 10);

    // Parse minimizer and its parameters.
    XML::Element minimizerElement = fittingElement.firstChildElement();
    if(!minimizerElement)
        throw runtime_error(str(format("Expected minimizer element in <%1%> element at line %2% of XML file.") %
                                fittingElement.tag() % fittingElement.lineNumber()));

    _fitMinimizer = Minimizer::createAndParse(minimizerElement, this, intervalForPrinting, true);
}

/******************************************************************************
* Parses the contents of the <deformations> element in the job file.
******************************************************************************/
void FitJob::parseDeformationsElement(XML::Element deformationsElement)
{
    // Parse definitions of deformations.
    for(XML::Element deformationElement = deformationsElement.firstChildElement(); deformationElement;
        deformationElement = deformationElement.nextSibling()) {
        deformationElement.expectTag("deformation");

        // Parse id.
        FPString id = deformationElement.parseStringParameterAttribute("id");

        // Parse scaling factor.
        double scaleFactor = deformationElement.parseOptionalFloatParameterElement("scale", 0.0);

        // Parse deformation matrix.
        Matrix3 deformation = Matrix3::Identity();
        XML::Element matrixElement = deformationElement.firstChildElement("matrix");
        if(matrixElement) {
            const char* vectorTagNames[3] = {"s1", "s2", "s3"};
            for(int index = 0; index < 3; index++) {
                XML::Element aElement = matrixElement.expectChildElement(vectorTagNames[index]);
                deformation.column(index).x() = aElement.parseFloatParameterAttribute("x");
                deformation.column(index).y() = aElement.parseFloatParameterAttribute("y");
                deformation.column(index).z() = aElement.parseFloatParameterAttribute("z");
            }
        }

        if(scaleFactor != 0) deformation = scaleFactor * deformation;

        _deformations.insert(make_pair(id, deformation));
    }
}

/******************************************************************************
* Returns the atom type index of the species specified in the given element attribute.
* Throws exception if species has not been defined.
******************************************************************************/
int FitJob::parseAtomTypeAttribute(XML::Element element, const char* attributeName)
{
    FPString speciesName = element.parseStringParameterAttribute(attributeName);
    for(int i = 0; i < numAtomTypes(); i++)
        if(atomTypeName(i) == speciesName) return i;

    throw runtime_error(str(format("Undefined atomic species (%1%) in %2% attribute of element <%3%> in line %4%.") %
                            speciesName % attributeName % element.tag() % element.lineNumber()));
}

/******************************************************************************
* Returns the atom type index of the species specified in the given element.
* Throws exception if species has not been defined.
******************************************************************************/
int FitJob::parseAtomTypeElement(XML::Element element, const char* elementName)
{
    FPString speciesName = element.parseStringParameterElement(elementName);
    for(int i = 0; i < numAtomTypes(); i++)
        if(atomTypeName(i) == speciesName) return i;

    throw runtime_error(str(format("Undefined atomic species (%1%) in sub-element <%2%> of element <%3%> in line %4%.") %
                            speciesName % elementName % element.tag() % element.lineNumber()));
}

/******************************************************************************
* Returns the list of atom types corresponding to the species specified in the given element attribute.
* Throws exception if some species has not been defined.
******************************************************************************/
vector<int> FitJob::parseAtomTypesAttribute(XML::Element element, const char* attributeName)
{
    vector<int> result;
    FPString speciesList = element.parseStringParameterAttribute(attributeName);
    if(speciesList == "*") {
        for(int i = 0; i < numAtomTypes(); i++) result.push_back(i);
    }
    else {
        std::vector<FPString> tokens;
        boost::split(tokens, speciesList, boost::is_any_of(","), boost::token_compress_on);
        for(const auto& speciesName : tokens) {
            bool found = false;
            for(int i = 0; i < numAtomTypes(); i++) {
                if(atomTypeName(i) == speciesName) {
                    result.push_back(i);
                    found = true;
                }
            }
            if(!found)
                throw runtime_error(
                    str(format("Atom species %1% in %2% attribute of element <%3%> in line %4% has not been defined.") %
                        speciesName % attributeName % element.tag() % element.lineNumber()));
        }
        if(result.empty())
            throw runtime_error(str(format("Empty species list in %1% attribute of element <%2%> in line %3%.") % attributeName %
                                    element.tag() % element.lineNumber()));
    }
    return result;
}

/******************************************************************************
* Returns the default tolerance for the given property based on the selected tolerance preset.
******************************************************************************/
double FitJob::getDefaultPropertyTolerance(FitProperty* property)
{
    if(_tolerancePreset == Uniform) {
        return 1.0;
    }
    else if(_tolerancePreset == Balanced) {
        if(property->units() == "eV")
            return 0.2;
        else if(property->units() == "eV/atom")
            return 0.01;
        else if(property->units() == "GPa")
            return 2.0;
        else if(property->units() == "A")
            return 0.005;
        else if(property->units() == "meV")
            return 10;
        else if(property->units() == "meV/A")
            return 10;
        else if(property->units() == "eV/A")
            return 0.01;
    }
    else if(_tolerancePreset == AccurateEnergies) {
        if(property->units() == "eV")
            return 0.1;
        else if(property->units() == "eV/atom")
            return 0.005;
        else if(property->units() == "GPa")
            return 2.0;
        else if(property->units() == "A")
            return 0.005;
        else if(property->units() == "meV")
            return 5;
        else if(property->units() == "meV/A")
            return 10;
        else if(property->units() == "eV/A")
            return 0.01;
    }
    return 1.0;
}

/******************************************************************************
* Returns the scalar fit property specified in the given element attribute.
* Throws exception if structure or property has not been defined.
******************************************************************************/
ScalarFitProperty* FitJob::parseScalarFitPropertyAttribute(XML::Element element, const char* attributeName)
{
    // Get and check the structure ID and the property ID.
    FPString propertyId = element.parseStringParameterAttribute(attributeName);
    auto pos = propertyId.find('.');
    if(pos == FPString::npos)
        throw runtime_error(
            str(format("Invalid reference in line %1% of XML file: STRUCTURE_ID.PROPERTY_ID expected") % element.lineNumber()));
    FPString structureId = propertyId.substr(0, pos);
    propertyId = propertyId.substr(pos + 1);

    // Check if the atomic structure exists.
    AtomicStructure* structure = getAtomicStructure(structureId);
    if(!structure)
        throw runtime_error(str(format("Invalid reference in line %1% of XML file: Non-existing structure \"%2%\".") %
                                element.lineNumber() % structureId));

    // Check if the property exists.
    ScalarFitProperty* property = dynamic_cast<ScalarFitProperty*>(structure->propertyById(propertyId));
    if(!property)
        throw runtime_error(str(format("Invalid reference in line %1% of XML file: Non-existing property \"%2%\".") %
                                element.lineNumber() % propertyId));
    return property;
}

/******************************************************************************
* Returns the property with the given ID.
******************************************************************************/
ScalarFitProperty* FitJob::getScalarFitProperty(const FPString& id)
{
    FPString propertyId = id;
    auto pos = propertyId.find('.');
    if(pos == FPString::npos) throw runtime_error(str(format("Invalid property ID \"%1%\": No \".\" found") % id));
    FPString structureId = propertyId.substr(0, pos);
    propertyId = propertyId.substr(pos + 1);

    // Check if the atomic structure exists.
    AtomicStructure* structure = getAtomicStructure(structureId);
    if(!structure)
        throw runtime_error(
            str(format("Invalid reference in property ID \"%1%\": Non-existing structure \"%2%\".") % id % structureId));

    // Check if the property exists.
    ScalarFitProperty* property = dynamic_cast<ScalarFitProperty*>(structure->propertyById(propertyId));
    if(!property)
        throw runtime_error(
            str(format("Invalid reference in property ID \"%1%\": Structure has no property \"%2%\".") % id % propertyId));
    return property;
}

/******************************************************************************
* Returns the atomic structure with the given ID.
******************************************************************************/
AtomicStructure* FitJob::getAtomicStructure(const FPString& id)
{
    for(AtomicStructure* s : structures()) {
        if(s->id() == id) return s;
    }
    return nullptr;
}

/******************************************************************************
* Returns the derived property with the given ID.
******************************************************************************/
DerivedProperty* FitJob::getDerivedProperty(const FPString& id)
{
    for(DerivedProperty* p : derivedProperties()) {
        if(p->id() == id) return p;
    }
    return nullptr;
}

/******************************************************************************
* Register a new structure with the job. It is added to the list of all atomic structures.
* This function is called by the AtomicStructure constructor.
******************************************************************************/
void FitJob::addStructure(AtomicStructure* structure)
{
    BOOST_ASSERT(structure);

    if(structure->id().find('.') != FPString::npos)
        throw runtime_error(str(format("Invalid structure ID: '%1%' contains '.'") % structure->id()));
    if(structure->id().find('[') != FPString::npos)
        throw runtime_error(str(format("Invalid structure ID: '%1%' contains '['") % structure->id()));

    if(getAtomicStructure(structure->id()) != nullptr)
        throw runtime_error(str(format("Duplicate structure ID detected: '%1%'") % structure->id()));

    _structures.push_back(structure);
}

/******************************************************************************
* Add a structure to the list of referenced structures.This function is called
* by the parseSetEqualToAttribute function of DegreeOfFreedom.
******************************************************************************/
void FitJob::addReferencedStructure(AtomicStructure* structure)
{
    // Check if the list already contains this structure.
    if(std::find(referencedStructures().begin(), referencedStructures().end(), structure) == referencedStructures().end()) {
        _referencedStructures.push_back(structure);
    }
}

/******************************************************************************
* Register a new derived property with the job. It is added to the list of all derived properties.
* This function is called by the DerivedProperty constructor.
******************************************************************************/
void FitJob::addDerivedProperty(DerivedProperty* derivedProperty)
{
    BOOST_ASSERT(derivedProperty);

    // Check if the id is valid
    if(derivedProperty->id().find('.') != FPString::npos)
        throw runtime_error(str(format("Invalid property ID: '%1%' contains '.'") % derivedProperty->id()));
    if(derivedProperty->id().find('[') != FPString::npos)
        throw runtime_error(str(format("Invalid property ID: '%1%' contains '['") % derivedProperty->id()));

    if(getDerivedProperty(derivedProperty->id()) != nullptr)
        throw runtime_error(str(format("Duplicate derived-property ID detected: '%1%'") % derivedProperty->id()));

    _derivedProperties.push_back(derivedProperty);
}

/******************************************************************************
* Returns the degree of freedom defined as path like str with "." instead of "/",
* currently searching in potentials
******************************************************************************/
DegreeOfFreedom* FitJob::DOFByPath(const FPString& pathId) const 
{   
    auto pos = pathId.find(".");
    if(pos!=FPString::npos) {
        FPString pot_id = pathId.substr(0, pos);
        for(FitObject* pot: _potentials) {
            if(pot->id() == pot_id) {
                return pot->DOFByPath(pathId.substr(pos+1));
            }
        }
    }
    throw runtime_error(str(format("Define full path to the dof split by '.', starting with the potential ID")));
}

FunctionBase* FitJob::functionByPath(const FPString& pathId) const 
{   
    auto pos = pathId.find(".");
    if(pos!=FPString::npos) {
        FPString pot_id = pathId.substr(0, pos);
        for(FitObject* pot: _potentials) {
            if(pot->id() == pot_id) {
                return pot->functionByPath(pathId.substr(pos+1));
            }
        }
    }
    throw runtime_error(str(format("Define full path to the function split by '.', starting with the potential ID")));
}

void FitJob::parseParameterConstraintsElement(XML::Element constraintElement) {
    for(XML::Element element = constraintElement.firstChildElement(); element;
        element = element.nextSibling()) {
            element.expectTag("constraint");
            ParameterConstraint* constraint;
            FPString id = element.parseStringParameterAttribute("id");
            constraint = new ParameterConstraint(id, this);
            constraint->parse(element);
            addParameterConstraint(constraint);
        }
}

void FitJob::addParameterConstraint(ParameterConstraint* constraint)
{
    _parameterConstraintList.push_back(constraint);
}

}
