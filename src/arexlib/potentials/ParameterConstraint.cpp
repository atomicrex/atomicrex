///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "ParameterConstraint.h"
#include "../Atomicrex.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"
#include "functions/Functions.h"
#include <muParser.h>

namespace atomicrex {

using namespace std;
using namespace boost;

void ParameterConstraint::parse(XML::Element element) {
    FPString dofPath = element.parseStringParameterAttribute("dependent-dof");
    _dependantDOF = dynamic_cast<ScalarDOF*>(job()->DOFByPath(dofPath));
    if(_dependantDOF->fitEnabled())
        throw runtime_error(
            str(format("dependent-dof %2% in %1% of XML file is not allowed to be enabled for fitting.") %
                    element.lineNumber() % dofPath));
    parseExpressionElement(element.expectChildElement("expression"));
    // Compute here to overwrite the value set when defining the dof before calculating properties the first time
    compute();
}

void ParameterConstraint::parseExpressionElement(XML::Element element) {
    FPString expr = element.textContent();
    // Boundaries of the variables.
    auto start = expr.find('{');
    auto end = expr.find('}');
    int ndof = 0;
    // Go through the equation and deal with all variables
    while(start != FPString::npos) {
        FPString dofPath = expr.substr(start + 1, end - start - 1);
        FPString variableName = getVariableName(dofPath);
        // replace the variables in the equation string.
        expr.replace(start, end - start + 1, variableName);
        // get boundaries of the next variable.
        start = expr.find('{');
        end = expr.find('}');
        }

    _dofValues.resize(_inputDOFs.size());
    for(size_t i = 0; i < _dofValues.size(); i++) {
        _dofValues[i] = *_inputDOFs[i];
        try {_parser.DefineVar(str(format("d_%1%") % i), &_dofValues[i]);}
        catch(mu::Parser::exception_type &e) {
            MsgLogger(medium) << "Exception while defining MuParser variable" << endl;
            MsgLogger(medium) << e.GetMsg() << endl;
            throw runtime_error("Aborting because of previous exception");
        }
    }

    _funcValues.resize(_inputFunctions.size());
    for(size_t i = 0; i < _funcValues.size(); i++) {
        _funcValues[i] = _inputFunctions[i](r_values[i]);
        try {_parser.DefineVar(str(format("f_%1%") % i), &_funcValues[i]);}
        catch(mu::Parser::exception_type &e) {
            MsgLogger(medium) << "Exception while defining MuParser variable" << endl;
            MsgLogger(medium) << e.GetMsg() << endl;
            throw runtime_error("Aborting because of previous exception");
        }
    }

    // Feed expression to the parser.
    try {_parser.SetExpr(expr);}
    catch(mu::Parser::exception_type &e) {
            MsgLogger(medium) << "Exception while setting MuParser expression" << endl;
            MsgLogger(medium) << e.GetMsg() << endl;
            throw runtime_error("Aborting because of previous exception");
    }
}

void ParameterConstraint::updateExpressionVariables() {
    for(size_t i = 0; i < _dofValues.size(); i++) {
        _dofValues[i] = *_inputDOFs[i];
        /// Since this is called very often and I am not sure if debugging level is checked everytime it is commented
        ///MsgLogger(debug) << "Updated d_" << i << " to " << *_inputDOFs[i] << " _dofValues" << i << " == " << _dofValues[i] << endl;
    }
    for(size_t i = 0; i < _funcValues.size(); i++) {
        _funcValues[i] = _inputFunctions[i](r_values[i]);
        /// Same here
        /// MsgLogger(debug) << "Updated f_" << i << " to " << _inputFunctions[i](r_values[i]) << ". r value is " <<  r_values[i] << endl;
    }
}
 
/// Get variable name. If the dofPath alread exists it returns the correct name, else it creates a new one
/// and registers the input dof or input function
FPString ParameterConstraint::getVariableName(FPString& dofPath)
{   
    if(dofPath.find(":") != FPString::npos) {
        int i = 0;
        for( ;i<_funcPaths.size(); i++){
            if(dofPath == _funcPaths[i]) break;
        }
        if(i == _funcPaths.size()) registerInputFunction(dofPath);
        return str(format("f_%1%") % i);
    } 
    /// check if dofPath exists in _dofPaths and get its index.
    int i = 0;
    for( ;i<_dofPaths.size(); i++){
        if(dofPath == _dofPaths[i]) break;
    }
    if(i == _dofPaths.size()) registerInputDOF(dofPath);
    return str(format("d_%1%") % i);
}

/// register the dof found using dofPath in the corresponding vectors.
void ParameterConstraint::registerInputDOF(FPString& dofPath)
{   
    _dofPaths.push_back(dofPath);
    ScalarDOF* dof = dynamic_cast<ScalarDOF*>(job()->DOFByPath(dofPath));
    if(dof == nullptr)
        throw runtime_error(
            str(format("Invalid reference in line XML file: Can't find degree of freedom %1%.") % dofPath));
    _inputDOFs.push_back(dof);
    dof-> addChangeListener([this](ScalarDOF& changingDOF) {
        compute();
        });
}

void ParameterConstraint::registerInputFunction(FPString& funcPath)
{
    _funcPaths.push_back(funcPath);
    auto pos = funcPath.find(":");
    // Get a pointer to the function identified 
    FunctionBase* func = job()->functionByPath(funcPath.substr(0, pos));
    FPString kw_r = funcPath.substr(pos+1);
    pos = kw_r.find(".");
    if(pos==FPString::npos)
        throw runtime_error((str(format("Ill formated function path %1%. It should be path:keyword.r_value") % funcPath)));
    FPString kw = kw_r.substr(0, pos);
    /// parse if the function value, derivative or 2nd derivative should be calculated
    if(kw == "eval") {
        _inputFunctions.push_back(
            [func](double r) {return func->evaluate(r);}
            );
            MsgLogger(debug) << "Registering " << funcPath << " deriv calculation" << endl;
    } else if (kw == "deriv"){
        _inputFunctions.push_back(
            [func](double r) {return func->evaluateDeriv(r);}
            );
            MsgLogger(debug) << "Registering " << funcPath << " deriv calculation" << endl;
    } else if (kw == "deriv2"){
        _inputFunctions.push_back(
            [func](double r) {return func->evaluateDeriv2(r);}
            );
            MsgLogger(debug) << "Registering " << funcPath << " deriv2 calculation" << endl;
    } else
        throw runtime_error((str(format("Unknown keyword %1% after ':'.It should be one of eval, deriv and deriv2") % kw)));

    /// parse r value from str
    double r;
    try{
        r = std::stod(kw_r.substr(pos+1));
    } catch(invalid_argument) {
        throw runtime_error((str(format("Invalid r value %1% encountered in %2%") % kw_r.substr(pos+1) % funcPath )));
    } catch(overflow_error){
        throw runtime_error((str(format("Overflowing r value %1% encountered in %2%") % kw_r.substr(pos+1) % funcPath )));
    } catch(const std::exception &e){
        throw runtime_error((str(format("Unexpected error %1% occured parsing r value %2% encountered in %3%") % e.what() % kw_r.substr(pos+1) % funcPath )));
    }

    MsgLogger(debug) << "Parsing r value from str " << kw_r.substr(pos+1) << endl;
    r_values.push_back(r);
    /// add the compute() method to _changeListeners of all dofs of the function
    std::vector<DegreeOfFreedom*> doflist;
    func->listAllDOF(doflist);
    for(DegreeOfFreedom* dof: doflist){
        ScalarDOF* sdof = dynamic_cast<ScalarDOF*>(dof);
        if(!dof)
            throw runtime_error((str(format("Could not transfer DegreeOfFreedom* to ScalarDOF* for %1%") % funcPath)));
        sdof-> addChangeListener([this](ScalarDOF& changingDOF) {
            compute();
        });
    }
    /// Alternative implementation idea if problems occur
    ///func->_changeListeners.push_back([this] {compute();});
}

void ParameterConstraint::compute() {
    updateExpressionVariables();
    try{
        auto val = _parser.Eval();
        MsgLogger(debug) << "Computing " << _parser.GetExpr() << " Result: " << val << endl;
        *_dependantDOF=val;
        MsgLogger(debug) << "Set " << _dependantDOF->id() << endl;
    }
    catch(mu::Parser::exception_type &e) {
        MsgLogger(medium) << "Exception while evaluating MuParser expression" << endl;
        MsgLogger(medium) << e.GetMsg() << endl;
        throw runtime_error("Aborting because of previous exception");
    }

}

}