///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "PairPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Computes the total energy of the structure.
 ******************************************************************************/
double PairPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;
    int inum = data.neighborList.numAtoms();
    int ntypes = job()->numAtomTypes();
    int ninteractions = _pairInteractions.size();

    BOOST_ASSERT(inum == structure.numLocalAtoms());

    // Compute pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        BOOST_ASSERT(i < structure.numLocalAtoms());
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            BOOST_ASSERT(jtype == structure.atomType(jlocal));
            for(int pi = 0; pi < ninteractions; pi++) {
                const PairInteraction& interaction = *_pairInteractions[pi];
                if(interaction.interacting(itype, jtype)) {
                    double V_AB = interaction.V()->evaluate(rij);
                    totalEnergy += V_AB;
                }
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
 * Computes the total energy and forces of the structure.
 ******************************************************************************/
double PairPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;
    int inum = data.neighborList.numAtoms();
    int ntypes = job()->numAtomTypes();
    int ninteractions = _pairInteractions.size();
    vector<Vector3>& forces = structure.atomForces();

    BOOST_ASSERT(inum == structure.numLocalAtoms());

    // Compute pair interactions.
    Vector3 fvec;
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        BOOST_ASSERT(i < structure.numLocalAtoms());
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            BOOST_ASSERT(jtype == structure.atomType(jlocal));
            for(int pi = 0; pi < ninteractions; pi++) {
                const PairInteraction& interaction = *_pairInteractions[pi];
                if(interaction.interacting(itype, jtype)) {
                    double V_AB_prime;
                    double V_AB = interaction.V()->evaluate(rij, V_AB_prime);
                    totalEnergy += V_AB;
                    Vector3 fvec = neighbor_j->delta / (rij * V_AB_prime);
                    BOOST_ASSERT(i < forces.size());
                    forces[i] -= fvec;
                }
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
 * This function is called by the fit job on shutdown, i.e. after the fitting
 * process has finished.
 ******************************************************************************/
void PairPotential::outputResults()
{
    Potential::outputResults();

    if(_exportPotentialFile.empty() == false) {
        MsgLogger(medium) << "Writing pair potential file to " << makePathRelative(_exportPotentialFile) << endl;
        writePotential(_exportPotentialFile);
    }

    if(_exportFunctionsFile.empty() == false) {
        MsgLogger(medium) << "Writing pair potential function tables to " << makePathRelative(_exportFunctionsFile) << endl;
        writeTables(_exportFunctionsFile);
    }
}

/******************************************************************************
 * Generates a potential file to be used with simulation codes.
 ******************************************************************************/
void PairPotential::writePotential(const FPString& filename) const
{
    ofstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Could not open potential file for writing: %1%") % filename));

    if(_MsgLoggerPrecision>=12){stream << setprecision(_MsgLoggerPrecision);}
    else{stream << setprecision(12);}

    // The fit job may contain more atom types than what we write to the potential file.
    // Thus, atom types need to be renumbered.
    // TODO This feature is currently NOT IMPLEMENTED
    int ntypes = 0;
    vector<int> typeMapping(job()->numAtomTypes());
    for(int atype = 0; atype < job()->numAtomTypes(); atype++) {
        //		if(isActive[atype])
        typeMapping[atype] = ++ntypes;
    }

    // Write format version line.
    stream << "PairPotential 1.0" << endl;

    // Write comment line.
    stream << job()->name() << " (" << boost::gregorian::day_clock::local_day() << ")" << endl;

    // Pair potentials.
    int nr = 200;
    double dr = cutoff() / (nr - 1);
    double leftDerivative, rightDerivative;
    stream << "Pair potentials" << endl;
    stream << _pairInteractions.size() << " " << cutoff() << " " << nr << " " << dr << endl;
    for(const std::shared_ptr<PairInteraction>& interaction : _pairInteractions) {
        stream << typeMapping[interaction->atomTypeA()] << " " << typeMapping[interaction->atomTypeB()] << " " << endl;
        stream << "iac " << interaction->id() << endl;
        interaction->V()->evaluate(0, leftDerivative);
        interaction->V()->evaluate(cutoff(), rightDerivative);
        stream << leftDerivative << " " << rightDerivative << endl;
        for(int i = 0; i < nr; i++) stream << interaction->V()->evaluate(dr * i) << " ";
        stream << endl;
    }
}

/******************************************************************************
 * Write potential tables to files.
 ******************************************************************************/
void PairPotential::writeTables(const FPString& basename) const
{
    double samplingResolution = 200.0;
    double rmin = 0.2, rmax = cutoff();
    double dr = (rmax - rmin) / samplingResolution;

    // Write pair potentials.
    for(const std::shared_ptr<PairInteraction>& interaction : _pairInteractions) {
        FPString filename = str(format("%1%.pair.%2%-%3%_%4%.table") % basename % job()->atomTypeName(interaction->atomTypeA()) %
                                job()->atomTypeName(interaction->atomTypeB()) % interaction->id());
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));
        interaction->V()->writeTabulated(out, rmin, rmax, dr);
    }
}

/******************************************************************************
 * Parses any potential-specific parameters in the XML element in the job file.
 ******************************************************************************/
void PairPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // Parse <interactions> block.
    parseInteractions(potentialElement.expectChildElement("interactions"));

    // Determine maximum cutoff.
    _cutoff = 0.0;
    for(const std::shared_ptr<PairInteraction>& interaction : _pairInteractions)
        _cutoff = std::max(interaction->V()->cutoff(), _cutoff);

    // Parse output options.
    _exportPotentialFile = potentialElement.parseOptionalPathParameterElement("export-potential");
    _exportFunctionsFile = potentialElement.parseOptionalPathParameterElement("export-functions");

    MsgLogger(maximum) << " Pair potential - maximum cutoff : " << _cutoff << endl;
}

/******************************************************************************
 * Parse pair interaction definitions in the XML element in the job file.
 ******************************************************************************/
void PairPotential::parseInteractions(XML::Element parentElement)
{
    // Iterate over all sub-elements in the XML file.
    for(XML::Element interactionElement = parentElement.firstChildElement(); interactionElement;
        interactionElement = interactionElement.nextSibling()) {
        interactionElement.expectTag("pair-interaction");

        // Get atom types to which this interaction applies.
        int atomTypeA = job()->parseAtomTypeAttribute(interactionElement, "species-a");
        int atomTypeB = job()->parseAtomTypeAttribute(interactionElement, "species-b");

        // Consistency check on atom types.
        if(!isAtomTypeEnabled(atomTypeA))
            throw runtime_error(str(format("Invalid atom type A in line %1% of XML file.") % interactionElement.lineNumber()));
        if(!isAtomTypeEnabled(atomTypeB))
            throw runtime_error(str(format("Invalid atom type B in line %1% of XML file.") % interactionElement.lineNumber()));

        // Parse pair-wise potential function.
        XML::Element potfuncElement = interactionElement.firstChildElement();
        if(!potfuncElement)
            throw runtime_error(
                str(format("<pair-interaction> element in line %1% of XML file must contain a function element.") %
                    interactionElement.lineNumber()));
        std::shared_ptr<FunctionBase> V = FunctionBase::createAndParse(potfuncElement, "pair", job());
        if(!V->hasCutoff())
            throw runtime_error(
                str(format("Pair potential function in line %1% of XML file does not have a valid cutoff radius.") %
                    potfuncElement.lineNumber()));

        // Check function.
        if(V->evaluate(V->cutoff()) != 0)
            throw runtime_error(str(format("Pair potential function in line %1% of XML file is not zero at cutoff radius.") %
                                    potfuncElement.lineNumber()));

        // Create interaction object.
        FPString interactionId = str(format("%1%-%2%") % job()->atomTypeName(atomTypeA) % job()->atomTypeName(atomTypeB));
        std::shared_ptr<PairInteraction> interaction(new PairInteraction(interactionId, job(), atomTypeA, atomTypeB, V));

        // Register interaction.
        registerSubObject(interaction.get());
        _pairInteractions.push_back(interaction);
    }
}

/******************************************************************************
 * Produces an XML representation of the potential's current parameter values
 * and DOFs that can be used as input in a subsequent fit job.
 ******************************************************************************/
XML::OElement PairPotential::generateXMLDefinition()
{
    XML::OElement root("pair-potential");

    XML::OElement interactions("interactions");
    root.appendChild(interactions);

    for(const std::shared_ptr<PairInteraction>& interaction : _pairInteractions) {
        XML::OElement interactionElement("pair-interaction");
        interactions.appendChild(interactionElement);
        interactionElement.setAttribute("species-a", job()->atomTypeName(interaction->atomTypeA()));
        interactionElement.setAttribute("species-b", job()->atomTypeName(interaction->atomTypeB()));

        XML::OElement potentialElement("potential");
        interactionElement.appendChild(potentialElement);

        XML::OElement potentialFuncElement = interaction->V()->generateXMLDefinition(interaction->V()->tag());
        potentialElement.appendChild(potentialFuncElement);
    }

    return root;
}
}
