///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AngularDependentPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Computes the total energy of the structure.
 ******************************************************************************/
double ADPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        if(isAtomTypeEnabled(itype) == false) continue;
        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        double mu[3] = {
            0.,
            0.,
            0.,
        };
        double lambda[6] = {
            0., 0., 0., 0., 0., 0.,
        };
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* rhoij = electronDensity(jtype)) rho_value += rhoij->evaluate(rij);
                if(FunctionBase* V = pairPotential(itype, jtype)) totalEnergy += 0.5 * V->evaluate(rij);
                // changed compared to eam
                if(FunctionBase* u = ufunction(itype, jtype)) {
                    double ur = u->evaluate(rij);
                    mu[0] += ur * neighbor_j->delta[0];
                    mu[1] += ur * neighbor_j->delta[1];
                    mu[2] += ur * neighbor_j->delta[2];
                }
                if(FunctionBase* w = wfunction(itype, jtype)) {
                    double wr = w->evaluate(rij);
                    lambda[0] += wr * neighbor_j->delta[0] * neighbor_j->delta[0];
                    lambda[1] += wr * neighbor_j->delta[1] * neighbor_j->delta[1];
                    lambda[2] += wr * neighbor_j->delta[2] * neighbor_j->delta[2];
                    lambda[3] += wr * neighbor_j->delta[1] * neighbor_j->delta[2];
                    lambda[4] += wr * neighbor_j->delta[0] * neighbor_j->delta[2];
                    lambda[5] += wr * neighbor_j->delta[0] * neighbor_j->delta[1];
                }
            }
        }
        if(FunctionBase* U = embeddingEnergy(itype)) totalEnergy += U->evaluate(rho_value);
        // changed compared to eam
        totalEnergy += 0.5 * mu[0] * mu[0];
        totalEnergy += 0.5 * mu[1] * mu[1];
        totalEnergy += 0.5 * mu[2] * mu[2];
        totalEnergy += 0.5 * lambda[0] * lambda[0];
        totalEnergy += 0.5 * lambda[1] * lambda[1];
        totalEnergy += 0.5 * lambda[2] * lambda[2];
        totalEnergy += lambda[3] * lambda[3];
        totalEnergy += lambda[4] * lambda[4];
        totalEnergy += lambda[5] * lambda[5];
        double nu = lambda[0] + lambda[1] + lambda[2];
        totalEnergy -= 1.0 / 6.0 * nu * nu;
    }
    return totalEnergy;
}

/******************************************************************************
 * Computes the total energy and forces of the structure.
 ******************************************************************************/
double ADPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    vector<Vector3>& forces = structure.atomForces();
    std::array<double, 6>& virial = structure.virial();

    // Reset per-atom array.
    ADPAtomData* perAtomData = data.perAtomData<ADPAtomData>();
    memset(perAtomData, 0., sizeof(ADPAtomData) * structure.numLocalAtoms());

    // Compute electron densities.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* rhoij = electronDensity(jtype)) perAtomData[i].rho += rhoij->evaluate(rij);
                if(FunctionBase* rhoji = electronDensity(itype)) perAtomData[neighbor_j->localIndex].rho += rhoji->evaluate(rij);
                // changed compared to eam
                if(FunctionBase* u = ufunction(itype, jtype)) {
                    double ur = u->evaluate(rij);
                    perAtomData[i].mu[0] += ur * neighbor_j->delta[0];
                    perAtomData[i].mu[1] += ur * neighbor_j->delta[1];
                    perAtomData[i].mu[2] += ur * neighbor_j->delta[2];

                    perAtomData[neighbor_j->localIndex].mu[0] -= ur * neighbor_j->delta[0];
                    perAtomData[neighbor_j->localIndex].mu[1] -= ur * neighbor_j->delta[1];
                    perAtomData[neighbor_j->localIndex].mu[2] -= ur * neighbor_j->delta[2];
                }
                if(FunctionBase* w = wfunction(itype, jtype)) {
                    double wr = w->evaluate(rij);
                    perAtomData[i].lambda[0] += wr * neighbor_j->delta[0] * neighbor_j->delta[0];
                    perAtomData[i].lambda[1] += wr * neighbor_j->delta[1] * neighbor_j->delta[1];
                    perAtomData[i].lambda[2] += wr * neighbor_j->delta[2] * neighbor_j->delta[2];
                    perAtomData[i].lambda[3] += wr * neighbor_j->delta[1] * neighbor_j->delta[2];
                    perAtomData[i].lambda[4] += wr * neighbor_j->delta[0] * neighbor_j->delta[2];
                    perAtomData[i].lambda[5] += wr * neighbor_j->delta[0] * neighbor_j->delta[1];

                    // Also + as done in Lammps. However, lammps source code also notes to verify
                    perAtomData[neighbor_j->localIndex].lambda[0] += wr * neighbor_j->delta[0] * neighbor_j->delta[0];
                    perAtomData[neighbor_j->localIndex].lambda[1] += wr * neighbor_j->delta[1] * neighbor_j->delta[1];
                    perAtomData[neighbor_j->localIndex].lambda[2] += wr * neighbor_j->delta[2] * neighbor_j->delta[2];
                    perAtomData[neighbor_j->localIndex].lambda[3] += wr * neighbor_j->delta[1] * neighbor_j->delta[2];
                    perAtomData[neighbor_j->localIndex].lambda[4] += wr * neighbor_j->delta[0] * neighbor_j->delta[2];
                    perAtomData[neighbor_j->localIndex].lambda[5] += wr * neighbor_j->delta[0] * neighbor_j->delta[1];
                }
            }
        }
    }

    // Compute U(rho) and U'(rho).
    for(int i = 0; i < structure.numLocalAtoms(); i++) {
        int itype = structure.atomType(i);
        FunctionBase* U = embeddingEnergy(itype);
        if(isAtomTypeEnabled(itype)) {
            if(U) totalEnergy += U->evaluate(perAtomData[i].rho, perAtomData[i].Uprime);

            totalEnergy += 0.5 * (perAtomData[i].mu[0] * perAtomData[i].mu[0] + perAtomData[i].mu[1] * perAtomData[i].mu[1] +
                                  perAtomData[i].mu[2] * perAtomData[i].mu[2]);
            totalEnergy +=
                0.5 * (perAtomData[i].lambda[0] * perAtomData[i].lambda[0] + perAtomData[i].lambda[1] * perAtomData[i].lambda[1] +
                       perAtomData[i].lambda[2] * perAtomData[i].lambda[2]);
            totalEnergy +=
                (perAtomData[i].lambda[3] * perAtomData[i].lambda[3] + perAtomData[i].lambda[4] * perAtomData[i].lambda[4] +
                 perAtomData[i].lambda[5] * perAtomData[i].lambda[5]);
            totalEnergy -= 1.0 / 6.0 * pow((perAtomData[i].lambda[0] + perAtomData[i].lambda[1] + perAtomData[i].lambda[2]), 2);
        }
    }

    // Compute two-body pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);

                double rho_prime_ij = 0;
                if(FunctionBase* rhoij = electronDensity(jtype)) rhoij->evaluate(rij, rho_prime_ij);

                double rho_prime_ji = 0;
                if(FunctionBase* rhoji = electronDensity(itype)) rhoji->evaluate(rij, rho_prime_ji);

                double pair_pot = 0;
                double pair_pot_deriv = 0;
                if(FunctionBase* V = pairPotential(itype, jtype)) pair_pot = V->evaluate(rij, pair_pot_deriv);

                // ADP
                double ur;
                double u_deriv = 0;
                if(FunctionBase* u = ufunction(itype, jtype)) ur = u->evaluate(rij, u_deriv);
                // ADP
                double wr;
                double w_deriv = 0;
                if(FunctionBase* w = wfunction(itype, jtype)) wr = w->evaluate(rij, w_deriv);

                double fpair = 0;
                int j = neighbor_j->localIndex;

                double deltax = neighbor_j->delta[0];
                double deltay = neighbor_j->delta[1];
                double deltaz = neighbor_j->delta[2];
                double deltamux = perAtomData[i].mu[0] - perAtomData[j].mu[0];
                double deltamuy = perAtomData[i].mu[1] - perAtomData[j].mu[1];
                double deltamuz = perAtomData[i].mu[2] - perAtomData[j].mu[2];
                double trdeltamu = deltamux * deltax + deltamuy * deltay + deltamuz * deltaz;
                double sumlambdaxx = perAtomData[i].lambda[0] + perAtomData[j].lambda[0];
                double sumlambdayy = perAtomData[i].lambda[1] + perAtomData[j].lambda[1];
                double sumlambdazz = perAtomData[i].lambda[2] + perAtomData[j].lambda[2];
                double sumlambdayz = perAtomData[i].lambda[3] + perAtomData[j].lambda[3];
                double sumlambdaxz = perAtomData[i].lambda[4] + perAtomData[j].lambda[4];
                double sumlambdaxy = perAtomData[i].lambda[5] + perAtomData[j].lambda[5];
                double trdeltalambda = (sumlambdaxx * deltax * deltax + sumlambdayy * deltay * deltay +
                                        sumlambdazz * deltaz * deltaz + 2.0 * sumlambdaxy * deltax * deltay +
                                        2.0 * sumlambdaxz * deltax * deltaz + 2.0 * sumlambdayz * deltay * deltaz);
                double nu = sumlambdaxx + sumlambdayy + sumlambdazz;
                double recip = 1 / rij;

                double adpx = (deltamux * ur + trdeltamu * u_deriv * deltax * recip +
                               2.0 * wr * (sumlambdaxx * deltax + sumlambdaxy * deltay + sumlambdaxz * deltaz) +
                               w_deriv * deltax * recip * trdeltalambda - 1.0 / 3.0 * nu * (w_deriv * rij + 2.0 * wr) * deltax);
                double adpy = (deltamuy * ur + trdeltamu * u_deriv * deltay * recip +
                               2.0 * wr * (sumlambdaxy * deltax + sumlambdayy * deltay + sumlambdayz * deltaz) +
                               w_deriv * deltay * recip * trdeltalambda - 1.0 / 3.0 * nu * (w_deriv * rij + 2.0 * wr) * deltay);
                double adpz = (deltamuz * ur + trdeltamu * u_deriv * deltaz * recip +
                               2.0 * wr * (sumlambdaxz * deltax + sumlambdayz * deltay + sumlambdazz * deltaz) +
                               w_deriv * deltaz * recip * trdeltalambda - 1.0 / 3.0 * nu * (w_deriv * rij + 2.0 * wr) * deltaz);

                if(isAtomTypeEnabled(itype)) {
                    fpair += rho_prime_ij * perAtomData[i].Uprime;
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                    // ADP contributions. I think this can be done more efficient
                    fpair += 0.5 * u_deriv * perAtomData[i].dipoleprime;
                    fpair += 0.5 * w_deriv * perAtomData[i].quadropoleprime;
                    fpair -= 1 / 6 * perAtomData[i].diagonalprime;
                }

                if(isAtomTypeEnabled(jtype)) {
                    fpair += rho_prime_ji * perAtomData[j].Uprime;
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                    // ADP contributions. I think this can be done more efficient
                    fpair += 0.5 * u_deriv * perAtomData[j].dipoleprime;
                    fpair += 0.5 * w_deriv * perAtomData[j].quadropoleprime;
                    fpair -= 1 / 6 * perAtomData[j].diagonalprime;
                }
                Vector3 fvec = neighbor_j->delta * (fpair / rij);
                fvec[0] += adpx;
                fvec[1] += adpy;
                fvec[2] += adpz;

                forces[i] += fvec;
                forces[j] -= fvec;

                virial[0] -= neighbor_j->delta.x() * fvec.x();
                virial[1] -= neighbor_j->delta.y() * fvec.y();
                virial[2] -= neighbor_j->delta.z() * fvec.z();
                virial[3] -= neighbor_j->delta.y() * fvec.z();
                virial[4] -= neighbor_j->delta.x() * fvec.z();
                virial[5] -= neighbor_j->delta.x() * fvec.y();
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
 * Parses any potential-specific parameters in the XML element in the job file.
 ******************************************************************************/
void ADPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // Parse list of functions.
    XML::Element functionsElement = potentialElement.expectChildElement("functions");
    for(XML::Element funcElement = functionsElement.firstChildElement(); funcElement; funcElement = funcElement.nextSibling()) {
        // Parse function object.
        std::shared_ptr<FunctionBase> func = FunctionBase::createAndParse(funcElement, FPString(), job());

        // Register function.
        registerSubObject(func.get());
        _functions.push_back(func);
    }

    _pairPotentialMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _electronDensityMap.resize(job()->numAtomTypes(), nullptr);
    _embeddingEnergyMap.resize(job()->numAtomTypes(), nullptr);
    _uMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _wMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);

    // Parse functional mapping.
    XML::Element mappingElement = potentialElement.expectChildElement("mapping");
    for(XML::Element element = mappingElement.firstChildElement(); element; element = element.nextSibling()) {
        if(element.tagEquals("pair-interaction")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("Pair potential function %1% of ADP potential %2% does not have a valid cutoff radius.") %
                        func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _pairPotentialMap[a * job()->numAtomTypes() + b] = func;
                    _pairPotentialMap[b * job()->numAtomTypes() + a] = func;
                }
            }
        }
        else if(element.tagEquals("electron-density")) {
            vector<int> speciesList = job()->parseAtomTypesAttribute(element, "species");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> element.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("Electron density function %1% of ADP potential %2% does not have a valid cutoff radius.") %
                        func->id() % id()));
            for(int a : speciesList) {
                _electronDensityMap[a] = func;
            }
        }
        else if(element.tagEquals("embedding-energy")) {
            vector<int> speciesList = job()->parseAtomTypesAttribute(element, "species");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            for(int a : speciesList) {
                _embeddingEnergyMap[a] = func;
            }
        }
        else if(element.tagEquals("u-function")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("u function %1% of ADP potential %2% does not have a valid cutoff radius.") % func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _uMap[a * job()->numAtomTypes() + b] = func;
                    _uMap[b * job()->numAtomTypes() + a] = func;
                }
            }
        }
        else if(element.tagEquals("w-function")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("w function %1% of ADP potential %2% does not have a valid cutoff radius.") % func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _wMap[a * job()->numAtomTypes() + b] = func;
                    _wMap[b * job()->numAtomTypes() + a] = func;
                }
            }
        }
        else
            throw runtime_error(str(format("Unknown ADP functional mapping element <%1%> in line %2% of XML file.") %
                                    element.tag() % element.lineNumber()));
    }

    // Determine maximum cutoff.
    _cutoff = 0;
    for(const auto& f : _pairPotentialMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }
    for(const auto& f : _electronDensityMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }
    for(const auto& f : _uMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }
    for(const auto& f : _wMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }

    // Parse output options.
    _exportFunctionsFile = potentialElement.parseOptionalPathParameterElement("export-functions");
    _exportADPFile = potentialElement.parseOptionalPathParameterElement("export-adp-file");
    if(!_exportADPFile.empty()) {
        XML::Element exportElement = potentialElement.firstChildElement("export-adp-file");
        _exportTableResolution = exportElement.parseOptionalIntParameterAttribute("resolution", _exportTableResolution);
        _rhoExportRangeFactor = exportElement.parseOptionalFloatParameterAttribute("rho-range-factor", _rhoExportRangeFactor);
    }

    MsgLogger(maximum) << " ADP potential - maximum cutoff : " << _cutoff << endl;
}

/******************************************************************************
 * This function is called by the fit job on shutdown, i.e. after the fitting
 * process has finished.
 ******************************************************************************/
void ADPotential::outputResults()
{
    Potential::outputResults();

    if(_exportFunctionsFile.empty() == false) {
        MsgLogger(medium) << "Writing ADP function tables to " << makePathRelative(_exportFunctionsFile) << ".*" << endl;
        writeTables(_exportFunctionsFile);
    }

    if(_exportADPFile.empty() == false) {
        MsgLogger(medium) << "Writing tabulated 'adp' file " << makePathRelative(_exportADPFile) << ":" << endl;
        writeADPFile(_exportADPFile);
    }
}

/******************************************************************************
 * Writes the tabulated functionals to a set of text files for visualization with Gnuplot.
 ******************************************************************************/
void ADPotential::writeTables(const FPString& basename) const
{
    double samplingResolution = 500.0;
    double rmin = 0.2, rmax = cutoff();
    double dr = (rmax - rmin) / samplingResolution;
    double rhomin = 0.0, rhomax = 2.5;
    double drho = (rhomax - rhomin) / samplingResolution;

    // Output functionals.
    for(const std::shared_ptr<FunctionBase>& func : _functions) {
        FPString filename = str(format("%1%.%2%.table") % basename % func->id());
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));

        // Is it a function used as embedding energy?
        if(std::find(_embeddingEnergyMap.begin(), _embeddingEnergyMap.end(), func.get()) != _embeddingEnergyMap.end()) {
            func->writeTabulated(out, rhomin, rhomax, drho);
        }
        else {
            func->writeTabulated(out, rmin, rmax, dr);
        }
    }
}

/******************************************************************************
 * Generates an 'adp' potential file to be used with other simulation codes like LAMMPS.
 ******************************************************************************/
void ADPotential::writeADPFile(const FPString& filename) const
{
    ofstream out(filename.c_str());
    if(!out.is_open()) throw runtime_error(str(format("Could not open ADP output file for writing: %1%") % filename));

    out << setprecision(16);
    out << "ADP file written by atomicrex [version " << ATOMICREX_VERSION_STRING << "]" << endl;
    out << "Fit job: " << job()->name() << endl;
    out << "Date: " << boost::gregorian::day_clock::local_day() << endl;

    // Collect list of elements that will be included in the output file.
    std::vector<int> activeSpecies;
    for(int a = 0; a < job()->numAtomTypes(); a++) {
        if(embeddingEnergy(a) != nullptr) {
            activeSpecies.push_back(a);
        }
    }
    if(activeSpecies.empty())
        throw runtime_error(str(
            format("Cannot export ADP potential '%1%' to a file because it does not define any embedding functions.") % id()));

    // Determine the maximum electron density occurring in any of the structures.
    double rhoCutoff = 0;
    for(AtomicStructure* structure : job()->structures()) {
        structure->updateStructure();
        double maxRho = computeMaximumElectronDensity(*structure, structure->perPotentialData(this));
        if(maxRho > rhoCutoff) rhoCutoff = maxRho;
    }
    if(rhoCutoff <= 0)
        throw runtime_error(str(
            format("Cannot export ADP potential '%1%' to a tabulated ADP file. Could not determine maximum electron density.") %
            id()));
    MsgLogger(medium) << "  Maximum electron density in all structures is " << rhoCutoff
                      << "; export range for embedding energy function is set to " << (rhoCutoff * _rhoExportRangeFactor) << endl;
    rhoCutoff *= _rhoExportRangeFactor;

    // Write element list.
    out << activeSpecies.size();
    for(int a : activeSpecies) {
        MsgLogger(medium) << "  Exporting tabulated ADP functions for atom type '" << job()->atomTypeName(a)
                          << "' (mass=" << job()->atomTypeMass(a) << ", atomic number=" << job()->atomTypeAtomicNumber(a) << ")"
                          << endl;
        out << " " << job()->atomTypeName(a);
    }
    out << endl;

    // Number of sampling points.
    int Nrho = _exportTableResolution;
    int Nr = _exportTableResolution;

    double drho = rhoCutoff / (Nrho - 1);
    double dr = cutoff() / (Nr - 1);
    out << Nrho << " " << drho << " " << Nr << " " << dr << " " << cutoff() << endl;

    // Write list of elements with embedding and electron density functions.
    for(int a : activeSpecies) {
        out << job()->atomTypeAtomicNumber(a) << " " << job()->atomTypeMass(a) << " 0.0 NONE" << endl;

        FunctionBase* embeddingFunc = embeddingEnergy(a);
        for(int i = 0; i < Nrho; i++) {
            double rho = drho * i;
            out << (embeddingFunc ? embeddingFunc->evaluate(rho) : 0.0) << "\n";
        }

        FunctionBase* rhoFunc = electronDensity(a);
        for(int i = 0; i < Nr; i++) {
            double r = dr * i;
            out << (rhoFunc ? rhoFunc->evaluate(r) : 0.0) << "\n";
        }
    }

    // Write pair potentials list.
    for(size_t i = 0; i < activeSpecies.size(); i++) {
        for(size_t j = 0; j <= i; j++) {
            FunctionBase* phiFunc = pairPotential(activeSpecies[i], activeSpecies[j]);
            for(int n = 0; n < Nr; n++) {
                double r = dr * n;
                out << ((phiFunc ? phiFunc->evaluate(r) : 0.0) * r) << "\n";
            }
        }
    }

    // Write u list.
    for(size_t i = 0; i < activeSpecies.size(); i++) {
        for(size_t j = 0; j <= i; j++) {
            FunctionBase* ufunc = ufunction(activeSpecies[i], activeSpecies[j]);
            for(int n = 0; n < Nr; n++) {
                double r = dr * n;
                out << (ufunc ? ufunc->evaluate(r) : 0.0) << "\n";
            }
        }
    }

    // Write w list.
    for(size_t i = 0; i < activeSpecies.size(); i++) {
        for(size_t j = 0; j <= i; j++) {
            FunctionBase* wfunc = wfunction(activeSpecies[i], activeSpecies[j]);
            for(int n = 0; n < Nr; n++) {
                double r = dr * n;
                out << (wfunc ? wfunc->evaluate(r) : 0.0) << "\n";
            }
        }
    }
}

/******************************************************************************
 * Computes the maximum electron density occurring in a structure.
 * This method is used by the writeADPFile() function to determine the range of the
 * embedding energy function to be tabulated.
 ******************************************************************************/
double ADPotential::computeMaximumElectronDensity(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double maxRho = 0;
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        if(isAtomTypeEnabled(itype) == false) continue;
        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* rhoij = electronDensity(jtype)) rho_value += rhoij->evaluate(rij);
            }
        }
        if(rho_value > maxRho) maxRho = rho_value;
    }
    return maxRho;
}
}  // namespace atomicrex
