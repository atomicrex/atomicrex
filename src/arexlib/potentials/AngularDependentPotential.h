///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "functions/Functions.h"

namespace atomicrex {

/**
   @brief This class defines angular dependent potentials (ADP) as described by Mishin in doi:10.1016/j.actamat.2005.05.001.
   @details The ADP is an extension to the EAM formalism. It includes 2 additional terms that can be interpreted as dipole and
   quadrupole distortions. \f[ E = \frac{1}{2}\sum_{ij} V(r_{ij}) + \sum_i F(\rho_i) +
   \frac{1}{2}\sum_{i,\alpha}(\mu_i^{\alpha})^2 + \frac{1}{2}\sum_{i, \alpha, \beta} (\lambda_i^{\alpha\beta})^2 -
   \frac{1}{6}\sum_i\nu_i^2 \f] where \f[ \rho_i= \sum_j \rho(r_{ij}), \f] \f[ \mu_i^{\alpha}=\sum_{j\neq i}
   u_{s_i}s_{s_j}(r_{ij})r_{ij}^{\alpha}, \f] \f[ \lambda_i^{\alpha\beta} = \sum_{j\neq i}
   w_{s_i}s_{s_j}(r_{ij})r_{ij}^{\alpha}r_{ij}^{\beta} \f] and \f[ \nu_i = \sum_{\alpha}=\lambda_i{\alpha\alpha} \f] is the trace
   of $\lambda$ The implementation is based on the EAM implementation in atomicrex and the ADP implemenation in lammps
 */
class ADPotential : public Potential
{
public:
    /// Constructor.
    ADPotential(const FPString& id, FitJob* job) : Potential(id, job, "ADP") {}

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Returns the number of bytes the potential needs per atom to store its intermediate
    /// calculation results during energy/force calculation.
    virtual size_t perAtomDataSize() const override { return sizeof(ADPAtomData); }

    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

    /// Returns the pair potential function for the interaction between the given atom types.
    FunctionBase* pairPotential(int speciesA, int speciesB) const
    {
        return _pairPotentialMap[speciesA * job()->numAtomTypes() + speciesB];
    }

    /// Returns the electron density function for the given pair of atom types.
    FunctionBase* electronDensity(int species) const { return _electronDensityMap[species]; }

    FunctionBase* ufunction(int speciesA, int speciesB) const { return _uMap[speciesA * job()->numAtomTypes() + speciesB]; }

    FunctionBase* wfunction(int speciesA, int speciesB) const { return _wMap[speciesA * job()->numAtomTypes() + speciesB]; }

    /// Returns the embedding energy function for the given atom type.
    FunctionBase* embeddingEnergy(int species) const { return _embeddingEnergyMap[species]; }

    /// Returns the function with the given ID, or nullptr if no such function is defined.
    FunctionBase* findFunctionById(const FPString& id) const
    {
        for(const auto& f : _functions)
            if(f && f->id() == id) return f.get();
        return nullptr;
    }

    /// This function is called by the fit job on shutdown, i.e. after the fitting process has finished.
    virtual void outputResults() override;

    /// Writes the tabulated functionals to a set of text files for visualization with Gnuplot.
    void writeTables(const FPString& basename) const;

    /// Generates an 'eam/fs' potential file to be used with other simulation codes like LAMMPS.
    void writeADPFile(const FPString& filename) const;

    /// Computes the maximum electron density occurring in a structure.
    /// This method is used by the writeEAMFile() function to determine the range of the
    /// embedding energy function to be tabulated.
    double computeMaximumElectronDensity(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const;

private:
    /// The list of analytical functions.
    std::vector<std::shared_ptr<FunctionBase>> _functions;

    /// The mapping of atom types to pair potential functions.
    std::vector<FunctionBase*> _pairPotentialMap;

    /// The mapping of atom types to electron density functions.
    std::vector<FunctionBase*> _electronDensityMap;

    /// The mapping of atom types to embedding energy functions.
    std::vector<FunctionBase*> _embeddingEnergyMap;

    std::vector<FunctionBase*> _uMap;

    std::vector<FunctionBase*> _wMap;

    /// The cutoff radius of the potential.
    double _cutoff;

    /// Table file that is written after fitting.
    FPString _exportFunctionsFile;

    /// ADP file that is written after fitting.
    FPString _exportADPFile;

    /// The desired resolution when writing the EAM potential to a tabulated data file.
    int _exportTableResolution = 500;

    /// Defines the cutoff range factor of the embedding functions. This is only used to
    /// write a tabulated version of the EAM potential. The maximum Rho value
    /// taken over all structures and atoms in the fit job is multiplied by this factor to
    /// determine the range of the embedding energy function to be tabulated.
    double _rhoExportRangeFactor = 2.0;

    /// Intermediate data structure used by computeEnergyAndForces().
    struct ADPAtomData {
        double rho, Uprime, dipoleprime, quadropoleprime, diagonalprime;
        double mu[3];
        double lambda[6];
    };
};

}  // namespace atomicrex
