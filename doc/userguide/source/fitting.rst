.. _training:
.. index:: Optimization algorithms, <fitting>

Optimization algorithms
**********************************

The ``<fitting>`` block in the :program:`atomicrex` input file controls
the optimization procedure. The tag can be supplemented by two
optional attributes as shown in the following example::

	<fitting enabled="true" output-interval="100">
	...
	</fitting>

Here, ``enabled`` can be used to selectively activate (``True``) or
deactivate (``False``) model optimization without the need to remove
the entire block.  The ``output-interval`` attribute sets the number
of iterations, between which the code reports its current status
(i.e., potential parameters, property data etc.).

At present :program:`atomicrex` *directly* supports the following
optimization algorithms:

* the :ref:`limited-memory Broyden-Fletcher-Goldfarb-Shanno (L-BFGS)
  minimizer <lbfgs>` with constraints
* the :ref:`Spa minimizer <spa>` for simply poking around
* the :ref:`split-Bregman minimizer <split_bregman>` (only useful for
  certain models)
* the algorithms provided by the :ref:`NLopt <nlopt>` library

There are several *optional* parameters, which are provided as
attributes within the minimizer tag (see below for examples). They
allow one to modify the criteria, which cause the minimization loop to
be terminated.

The first parameter ``conv-threshold`` defines a condition for the
maximum variation of the :ref:`objective function
<objective_function>` between successive iterations :math:`k` and
:math:`k+1`. The minimization is halted if

.. math::

         \frac{\chi^2_k - \chi^2_{k+1}}{\max\left\{\left|\chi^2_k\right|,\left|\chi^2_{k+1}\right|,1\right\}} \leq c_t \, p_m

where :math:`c_t` is set by ``conv-threshold`` and :math:`p_m` is the
machine precision, which is automatically generated by the code. The
default value is :math:`10^{-5}`.

If the number of iterations exceeds ``max-iter`` the optimization is
terminated. The default value is 100.

The gradient of the objective function is evaluated numerically using
central differences:

.. math::

   \left.\frac{d\chi^2}{dx}\right|_{x_0}
   \approx
   \frac{\chi^2(x_0+\varepsilon)
   - \chi^2(x_0-\varepsilon)}{2\varepsilon}
   + \mathcal{O}(\varepsilon^2)

The step width :math:`\varepsilon` is specified using the attribute
``gradient-epsilon`` and defaults to :math:`10^{-6}`.


.. _lbfgs:

.. index::
   single: Broyden-Fletcher-Goldfarb-Shanno (BFGS) minimizer
   single: Minimizers; Broyden-Fletcher-Goldfarb-Shanno (BFGS)
   single: <BFGS>

L-BFGS minimizer
====================================

:program:`atomicrex` includes an implementation of the limited-memory
Broyden-Fletcher-Goldfarb-Shanno (L-BFGS) minimizer, which is a
popular quasi-Newton method, with support for constraints. An
extensive description of the algorithm can be found on `this Wikipedia
page <https://en.wikipedia.org/wiki/Limited-memory_BFGS>`_.  The
minimizer can be activated by inserting the ``<BFGS>`` tag in the
``<fitting>`` block of the main input file with options provided via
attributes as illustrated by the following code block::

      <fitting>
           <BFGS conv-threshold="1e-16" max-iter="200" gradient-epsilon="1e-8" />
      </fitting>

where the default values for the optional parameters described above
have been overriden.


.. _spa:

.. index::
   single: Spa minimizer
   single: Minimizers; Spa minimizer
   single: <spa>

Spa minimizer
====================================

The Spa minimizer is actually not a separate optimization algorithm
but rather works as a wrapper. It generates randomized initial
parameter sets and then invokes another minimization algorithm to
optimize the starting parameter set.  The following code block
illustrates the use of the Spa minimizer. A complete example can be
found :ref:`here <example_fitting_spa_minimizer>`::

  <fitting>
    <spa max-iter="25" seed="120815">
      <BFGS conv-threshold="1e-10" max-iter="50" />
    </spa>
  </fitting>

The ``max-iter`` attribute specifies the number of times a new initial
parameter set is generated. The ``seeds`` attribute allows one to set
the seed of the random number generator. Here, the L-BFGS minimizer
:ref:`described above <lbfgs>` is used. Note that the maximum number
of iterations for the latter is set to a rather low value. This
procedure is recommended to avoid spending many iterations in the
inner loop on incrementally optimizing a parameter set rather than
sampling a larger number of different initial parameter sets. If a
parameter set has been found, whose :ref:`objective function
<objective_function>` is smaller than for any of the previously found
sets, the new set is written to file and, depending on the value of
the ``<verbosity>`` tag, standard output.

.. _spa_bounds:

.. warning::

   When using the Spa minimizer it is *strongly* advised to define
   also bounds for properties such as the lattice
   parameters. Otherwise the fit can run quickly away and yield
   unreasonable results. Bounds on the properties are imposed by using
   the ``min`` and ``max`` keywords of :ref:`the respective element
   <property_bounds>`.

.. _global_vs_local_minimizers:

.. warning::

   Note that optimizers that attempt a "global" minimum search
   including the Spa minimizer as well as the global algorithms from
   the :ref:`NLopt <nlopt>` library usually do *not* minimize
   individual points very carefully. It is therefore usually strongly
   advised to optimize the minima from these algorithms further using
   conventional (local) minimizers with tight convergence criteria.

.. _nlopt:

.. index::
   single: NLopt minimizer
   single: Minimizers; NLopt minimizer
   single: Minimizers; Global minimizer
   single: <nlopt>

Optimization via NLopt
=============================

:program:`atomicrex` features an interface to the `open-source library
NLopt for nonlinear optimization
<http://ab-initio.mit.edu/wiki/index.php/NLopt>`_, which provides a
large number of `algorithms
<http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms>`_ for both
global optimization, local derivative-free optimization, and local
gradient-based methods. These algorithms can be invoked as illustrated
by the following example. A complete example can be found :ref:`here
<example_fitting_nlopt_minimizer>`::

   <fitting>
      <nlopt algorithm="LN_NELDERMEAD" stopval="1e-10"/>
   </fitting>

The algorithm is selected using the mandatory ``algorithm`` attribute,
which can assume any of the following string values:

Local gradient based algorithms:

* ``LD_LBFGS``:
  `limited-memory Broyden-Fletcher-Goldfarb-Shanno
  (L-BFGS) algorithm
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#Low-storage_BFGS>`_
* ``LD_MMA``: `Method of moving asymptotes (MMA) and conservative convex
  separable approximation (CCSA)
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#MMA_.28Method_of_Moving_Asymptotes.29_and_CCSA>`_

Local, derivative free algorithms:

* ``LN_NELDERMEAD``: `Nelder-Mead Simplex
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#Nelder-Mead_Simplexz>`_
* ``LN_SBPLX``: `Sbplx (based on Subplex)
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#Sbplx_.28based_on_Subplex.29>`_

Global algorithms

* ``GN_CRS2_LM``: `Controlled random search (CRS) with local mutation
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#Controlled_Random_Search_.28CRS.29_with_local_mutation>`_
* ``GN_ESCH``: `ESCH (evolutionary algorithm)
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#ESCH_.28evolutionary_algorithm.29>`_
* ``GN_DIRECT``: `DIRECT and DIRECT-L
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#DIRECT_and_DIRECT-L>`_
* ``GN_ISRES``: `ISRES (Improved Stochastic Ranking Evolution Strategy)
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms#ISRES_.28Improved_Stochastic_Ranking_Evolution_Strategy.29>`_

.. _global_bounds:

.. warning::

   Please note some of the algorithms in NLopt, in particular most of
   the global-optimization algorithms, `do not support unconstrained
   optimization and will return an error if the user has not supplied
   finite lower and upper bounds
   <http://ab-initio.mit.edu/wiki/index.php/NLopt_Reference#Bound_constraints>`_.
   **Bounds for potential parameters** are set using the ``min`` and
   ``max`` attributes of the elements of the ``<fit-dof>`` block as
   described :ref:`here <fitdof>`.

   It is furthermore `strongly` advised to define also **bounds for
   properties** such as the lattice parameters. Otherwise the fit can
   run quickly away and yield unreasonable results. Bounds on the
   properties are imposed by using the ``min`` and ``max`` keywords of
   :ref:`the respective element <property_bounds>`.

The ``<nlopt>`` element can be further modified using several optional
attributes:

* ``seed`` (int): Set seed for pseudo-random number generator; more
  information can be found `here
  <http://ab-initio.mit.edu/wiki/index.php/NLopt_Reference#Pseudorandom_numbers>`_.

`Stopping criteria
<http://ab-initio.mit.edu/wiki/index.php/NLopt_Reference#Stopping_criteria>`_:

* ``stopval`` (float): Stop when the objective function :math:`\leq`
  ``stopval``.
* ``maxeval`` (int): Stop when the number of function evaluations
  exceeds ``maxeval``.
* ``maxtime`` (float): Stop when the optimization time (in seconds)
  exceeds ``maxtime``.
* ``ftol_rel`` (float): Stop when an optimization step (or an estimate
  of the optimum) changes the objective function value by less than
  ``ftol_rel`` multiplied by the absolute value of the function value.
* ``ftol_abs`` (float): Stop when an optimization step (or an estimate
  of the optimum) changes the function value by less than ``ftol_abs``.
* ``xtol_rel`` (float): Stop when an optimization step (or an estimate
  of the optimum) changes every parameter by less than ``xtol_rel``
  multiplied by the absolute value of the parameter.


.. _split_bregman:

.. cmt:

   .. index::
      single: Split-Bregman minimizer
      single: Minimizers; Split-Bregman
      single: <split-bregman>

   Split-Bregman minimizer
   ====================================

   The split-Bregman algorithm allows one to carry out an
   :math:`l_1`-norm optimization. It is suitable only for selected
   potential forms, namely at present `force constant models
   <fourth_order_model>`_. These features will be documented in the
   future, once the implementation and testing has been completed.
