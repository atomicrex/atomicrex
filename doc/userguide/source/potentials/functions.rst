.. _function_definition:

.. index::
   single: Function definition
   single: Defining a function
   single: <functions>

Defining functions
=======================================

Functions are used for the definition of the functional form of
several potential types, e.g., :ref:`embedded atom method (EAM)
potentials <eam_potential>`. :program:`atomicrex` provides several
predefined functions as well as a mechanism to defined (almost)
arbitrary functional forms. Please consult the respective potential
sections for information regarding the positioning of a function block
in the potential block. All function elements have an ``id`` attribute
that is used to identify the respective function, e.g., for assigning
functions to different interaction types for :ref:`EAM potentials
<eam_potential>` via the ``<mapping>`` block.

.. _screening_functions:

.. index::
   single: Screening functions
   single: Functions; screening functions

Screening functions
---------------------

Several pre-defined functions are intended to serve as "screening" (or
cutoff) functions. They are constructed to satisfy the conditions
:math:`f(0) = 1` and :math:`f(r_c) = 0` with (usually) smooth
derivatives at the boundaries [#cutoff_abop]_. These functions are
(the normalization factor that enforces :math:`f(0) = 1` has been
omitted for clarity):

.. index:: <exp-A>
   single: Functions; exponential screening

* ``<exp-A>``: This element sets up a type of exponential screening
  function,

  .. math::
     f(r; r_c) = \exp\left[ 1 / \left(r-r_c\right) \right],

  that is declared as follows::

    <exp-A>
      <cutoff> 5.5 </cutoff>
    </exp-A>

  Here, ``<cutoff>`` = :math:`r_c`.

.. index:: <exp-B>
   single: Functions; exponential screening

* ``<exp-B>``: This element sets up another type of exponential screening
  function,

  .. math::
     f(r; r_c; n; \alpha; r_c^{(i)}) = \exp\left[ -\text{sign}(n) \cdot \alpha \big/ \left(1-x^n\right) \right]

  where :math:`x=\left(r - r_c^{(i)}\right) \big/ \left( r_c -
  r_c^{(i)} \right)`. This screening function is declared as follows::

    <exp-B>
      <cutoff>    5.5 </cutoff>
      <rc>        2.8 </rc>
      <alpha>     1.0 </alpha>
      <exponent> -3.0 </exponent>
    </exp-B>

  Here, ``<cutoff>`` = :math:`r_c`, ``<rc>`` = :math:`r_c^{(i)}` (the
  inner cutoff radius, see :ref:`figure below
  <figure_screening_functions>`), ``<alpha>`` = :math:`\alpha`, and
  ``<exponent>`` = :math:`n`.

.. index:: <exp-gaussian>
   single: Functions; Gaussian screening

* ``<exp-gaussian>``: This element sets up a Gaussian distribution
  function multiplied with an exponential cutoff function

  .. math::
     f(r; r_c, n, \alpha, \sigma)
     = \exp\left[ -\text{sign}(n)
     \cdot \alpha \big/ \left(1-\left(r/r_c\right)^n\right) \right]
     \cdot \exp\left[ - r^2\big/2\sigma^2 \right] \big / \sigma\sqrt{2\pi}.

  The function is declared as follows::

    <exp-gaussian>
      <cutoff>    5.5 </cutoff>
      <stddev>    2.8 </stddev>
      <alpha>     1.0 </alpha>
      <exponent> -3.0 </exponent>
    </exp-gaussian>

  Here, ``<cutoff>`` = :math:`r_c`, ``<stddev>`` = :math:`\sigma`,
  ``<alpha>`` = :math:`\alpha`, and ``<exponent>`` = :math:`n`.
  
The following figure illustrates the different screening functions
using exemplary parameters.

.. _figure_screening_functions:

.. figure:: figs/screening_functions.png
    :width: 400
    :align: center

    Screening functions using exemplary parameters.

.. index::
   single: Interpolation functions
   single: Functions; interpolation functions

Interpolation functions
--------------------------

Another set of functions is intended as interpolation functions and
includes:

.. index:: Functions; polynomial
   single: <poly>

* ``<poly>``: A simple polynomial function defined as

  .. math::
     g(x) = \sum_{n=0}^P a_n x^n

  with the boundary conditions :math:`f(0) = 1` and :math:`f(r_c) =
  0`, where :math:`r_c` is the upper cutoff, can be set up as
  follows::

    <poly>
      <cutoff>5.5</cutoff>
      <coefficients>
        <coeff n="1" value="2.3" enabled="True" reset="False" min="1.0" max="7.0">
        <coeff n="2" value="1.1" enabled="True" reset="False" min="0.0" max="4.0">
        <coeff n="3" value="0.6" enabled="True" reset="False" min="0.0" max="4.0">
      </coefficients>
    </poly>

  Here, ``<cutoff>`` defines the cutoff. The ``<coefficients>`` block
  specifies the coefficients. The value of :math:`a_n` is set via the
  ``value`` attribute while the ``n`` attribute specifies :math:`n`. The
  attributes ``enabled``, ``reset``, ``min``, and ``max`` implement the usual
  DOF features described :ref:`here <fitdof>`. The zero-th order
  coefficient :math:`a_0` cannot be set by the user as it is fixed by
  the boundary conditions.

.. _spline_function:
.. index:: Functions; spline
   single: <spline>

* ``<spline>``: A `natural cubic spline
  <https://en.wikipedia.org/wiki/Spline_interpolation>`_ function can
  be declared as follows::

      <spline>
        <cutoff>5.5</cutoff>
	<nodes>
	  <node x="0"    y="10"   enabled="true" />
	  <node x="0.2"  y="-0.3" enabled="true" />
	  <node x="0.5"  y=" 0.1" enabled="true" />
	  <node x="0.7"  y="-0.1" enabled="true" />
	  <node x="0.85" y=" 0.4" enabled="true" />
	  <node x="5.5"  y=" 0"   enabled="true" />
	</nodes>
      </spline>

  Here, ``<cutoff>`` defines the cutoff. The ``<nodes>`` block specifies
  the nodal points of the spline. Each node is associated with a pair
  :math:`(x,y)` provided by the attributes ``x`` and ``y``. The attributes
  ``enabled``, ``reset``, ``min``, and ``max`` implement the usual DOF
  features described :ref:`here <fitdof>`. Note that only the
  :math:`y` value is varied during optimization while the :math:`x`
  value is kept fixed.

  It is possible to specify the first derivatives at the end points by
  using the optional parameter attributes ``derivative-left`` and
  ``derivative-right`` as follows::
	
      <spline>
        <derivative-left> 123.0 </derivative-left>
        <derivative-right> -456.0 </derivative-right>
        ...
      </spline>

  If these attributes are left unspecified they default to zero. An
  example for the use of splines can be found :ref:`here
  <example_potential_meam>`.
   
.. index:: Functions; constant
   single: <constant>

* ``<constant>``: A constant "function" can be set up using::
    
      <constant> 2.1 </constant>


.. index::
   single: Specialized functions
   single: Functions; specialized
   single: <fit-dof>
   single: <screening>

Specialized functions
------------------------

:program:`atomicrex` includes a few functions that are immediately
suitable for instance as functional forms for pair potentials.

.. index:: Functions; Morse potential form
   single: <morse-A>

* ``<morse-A>``: The original `Morse potential
  <https://en.wikipedia.org/wiki/Morse_potential>`_ (type ``A``) is
  defined as

  .. math::
     V(r) = D_0 \left[ \exp\left(-2 \alpha (r-r_0) \right) - 2 \exp\left(-\alpha (r-r0) \right) \right]

  where the parameters :math:`D_0` and :math:`r_0` determine the
  cohesive energy and the optimum bond length, respectively, while the
  parameter :math:`\alpha` affects the curvature of the potential
  curve and thus the stiffness. Note that the Morse potential has no
  "built-in" cutoff whence it must be combined with a :ref:`screening
  (cutoff) function <screening_functions>`. The following block
  illustrates the specification of a standard Morse potential in the
  input file. It also provides an example for :ref:`how to specify the
  fit parameters <fitdof>` (``<fit-dof>`` blocks) and the insertion of a
  :ref:`screening function <screening_functions>` via a ``<screening>``
  block::

    <morse-A>
      <D0>   0.3 </D0>
      <r0>   3.0 </r0>
      <beta> 1.8 </beta>
      <fit-dof>
        <D0   enabled="true" />
        <r0   enabled="true" />
        <beta enabled="false" />
      </fit-dof>

      <screening>
        <exp-B>
          <cutoff>    5.5 </cutoff>
          <rc>        0.0 </rc>
          <alpha>     1.0 </alpha>
          <exponent> -3.0 </exponent>
          <fit-dof>
            <alpha    enabled="false" />
            <exponent enabled="false" />
          </fit-dof>
        </exp-B>
      </screening>
    </morse-A>

.. index:: Functions; Morse potential form
   single: <morse-B>

* ``<morse-B>``: A generalized Morse potential (variant ``B``) as used for
  example in :ref:`analytic bond-order potentials <abop_potential>`
  but also in some :ref:`embedded atom method (EAM) potentials
  <eam_potential>` [MisMehPap01]_ is available as well. It is defined
  as

  .. math::
      V(r) =
      \underbrace{\frac{D_0}{S-1} \exp\left(- \beta \sqrt{2 S} (r-r_0) \right)}_{V_R}
      -
      \underbrace{\frac{D_0 S}{S-1} \exp\left(- \beta \sqrt{2/S} (r-r_0) \right)}_{V_A}
      +
      \delta.
  
  As in the original Morse potential, the parameters :math:`D_0` and
  :math:`r_0` determine the cohesive energy and the optimum bond
  length, respectively. The parameter :math:`\beta` affects the
  curvature of the potential curve and thus the stiffness. The
  :math:`S` parameter modifies the anharmonicity and can be set via
  the slope of the Pauling relation [AlbNorAve02]_, [AlbNorNor02]_,
  [ErhAlb05]_. The entire potential can be shifted using the
  :math:`\delta` parameter. With :math:`S=2` and :math:`\delta=0`, one
  recovers the original Morse potential.

The following block illustrates the specification of a modified Morse
potential (variant ``B``) in the input file::

    <morse-B>
      <D0>   0.3 </D0>
      <r0>   3.0 </r0>
      <beta> 1.8 </beta>
      <S>    2.1 </S>
    </morse-B>

.. index:: Functions; Morse potential form
   single: <morse-C>

* ``<morse-C>``: A generalized Morse potential (variant ``C``) in the
  style used in the :ref:`Tersoff potential <tersoff_potential>` is
  available as well. It is defined as

  .. math::
      \underbrace{A \exp\left(- \lambda r \right)}_{V_R}
      -
      \underbrace{B \exp\left(- \mu r \right)}_{V_A}
      +
      \delta.

  Note that the ``<morse-B>`` form is usual preferred since the
  parameters have an immediate physical interpretation. Also the
  parameters :math:`A` and :math:`B` in the ``<morse-C>`` form can vary
  over a very large range, occasionally causing numerical
  instabilities that can strongly interfere with the fitting
  process. The format of a ``<morse-C>`` block is equivalent to the
  definition of ``<morse-A>`` and ``<morse-B>`` functions.

.. index:: Functions; Gaussian function
   single: <gaussian>

* ``<gaussian>``: The Gaussian function is defined as

  .. math::
     V(r) = a \exp\left(-\eta (r-\mu)^2 \right)

  with the three parameters :math:`a` (the prefactor), :math:`\eta` and :math:`\mu`. 
  Note that the Gaussian function has no "built-in" cutoff, but -as for any function-  
  you can still specify a hard cutoff the optional ``cutoff`` attribute or apply
  a screening function using the ``<screening>`` element. 
  The following block exemplifies the specification of a Gaussian function in the
  input file. 
  block::

    <gaussian>
      <prefactor> 1.0 </prefactor>
      <eta> 3.0 </eta>
      <mu> 1.8 </mu>
      <fit-dof>
        <prefactor enabled="true"  />
        <eta       enabled="false" />
        <mu        enabled="true"  />
      </fit-dof>
    </gaussian>

.. index::
   single: Combination functions
   single: Functions; product
   single: Functions; sum
   single: <product>
   single: <sum>

Combination functions
------------------------

:program:`atomicrex` provides furthermore two convenience functions,
``<sum>`` and ``<product>``, which allow one to combine two or more
function blocks without the need to explicitly integrate them into
one. For example, one could add two screening type functions using the
following set up::

  <sum>
    <exp-A>
      <cutoff> 5.5 </cutoff>
    </exp-A>

    <exp-B>
      <cutoff>    5.5 </cutoff>
      <rc>        2.8 </rc>
      <alpha>     1.0 </alpha>
      <exponent> -3.0 </exponent>
    </exp-B>
  </sum>

The ``<sum>`` operation is used in the example that demonstrates the
construction of an :ref:`EAM potential with user defined functions
<example_potential_EAM>` to combine two pair
potentials.

.. _user_defined_functions:

.. index::
   single: User defined functions
   single: Functions; user defined
   single: <user-function>

User defined functions
-------------------------

:program:`atomicrex` links to the `muparser
<http://muparser.beltoforion.de/>`_ math parser library, which
provides enormous flexibility for defining functional forms. The
following input file block illustrates the definition of actually two
such user-defined function (one for the pair potential ``V`` and one for
the screening function ``rho_screening``)::

   <user-function id="V">
     <input-var> r </input-var>
     <expression> A*exp(-lambda*r) </expression>
     <derivative> -lambda*A*exp(-lambda*r) </derivative>
     <param name="A"> 500 </param>
     <param name="lambda"> 2.73 </param>
     <fit-dof>
       <A/>
       <lambda/>
     </fit-dof>

     <screening>
       <user-function id="rho_screening">
         <cutoff>6.5</cutoff>
         <input-var>r</input-var>
         <expression> 1 - 1/(1 + ((r - cutoff) / h)^4) </expression>
         <derivative>
           4 * h^4 * (r-cutoff)^3 / ((h^4 + (r-cutoff)^4)^2)
	 </derivative>
         <param name="h">3</param>
       </user-function>
     </screening>
   </user-function>

.. index::
   single: <input-var>
   single: <expression>
   single: <derivative>
   single: <param>
   single: <fit-dof>
   single: <screening>

Here, the following elements are used:

* ``<input-var>``: The identifier for the input variable used in the
  function definition.
* ``<expression>``: The actual function, where the identifier specified
  in ``<input-var>`` is used as the argument of the function.
* ``<derivative>``: The analytic derivative of the function specified in
  ``<expression>``.
* ``<param>``: Introduce a parameter that appears in the function
  definition. The name of the parameter is specified using the ``name``
  attribute. Typically there are several parameters.
* ``<fit-dof>``: This element is used to declare the parameters that are
  to be adjusted during the optimization process. The parameters are
  provided as a list of subelements of the ``<fit-dof>`` block, where
  the name of the element is simply the name provided via the ``name``
  attribute in the ``<param>`` element.
* ``<screening>``: The screening block allows one to integrate a
  screening (or cutoff) function, which provides a container for a
  function object.

A complete input file can be found in the example that demonstates the
construction of an :ref:`EAM potential with user defined functions
<example_potential_EAM>`.

.. rubric:: Footnotes

.. [#cutoff_abop] Some potentials such as :ref:`Tersoff
                  <tersoff_potential>` and :ref:`analytic bond-order
                  <abop_potential>` potentials have built-in cutoff
                  functions that are handled directly by the
                  potential routine.
