.. _example_fitting_eam_potential:

.. index::
   single: Embedded atom method (EAM); example
   single: Examples; Embedded atom method (EAM)
   single: Examples; User defined functions
   single: User defined functions; example
   single: Examples; User defined structures
   single: User defined structures; example

Fitting a simple embedded atom method (EAM) potential for Al
======================================================================================================

Using the `prepare_configurations_for_force_matching.py` script that
can be found in the `utility_scripts` directory, POSCAR files
containing forces were generated from the `.traj` files in the
`additional_data` directory. The :ref:`embedded atom method (EAM)
potential <eam_potential>` format used here is a special case of the
:ref:`Tersoff potential <tersoff_potential>` with a purely repulsive
pair potential

.. math::
   V(r) = A \exp(-\lambda r),

the embedding function

.. math::
   F(\rho) = -D \exp(-\rho),

and the electron density

.. math::
   \rho(r) = \exp(-2\mu r).

The four parameters are fitted to 108 atom face-centered cubic (FCC)
configurations at the equilibrium lattice parameter as well as 10%
compression and expansion. Also included in the fit is the Al bulk
modulus and the FCC lattice parameter.


Location
------------------

`examples/fitting_EAM_potential`


Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/structures.xml
       :linenos:
       :language: xml

* `POSCAR_res_POSCAR_0.9.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/POSCAR_res_POSCAR_0.9.traj_forces
       :linenos:
       :language: text

* `POSCAR_res_POSCAR_1.0.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/POSCAR_res_POSCAR_1.0.traj_forces
       :linenos:
       :language: text

* `POSCAR_res_POSCAR_1.1.traj_forces`: file with input configuration
  and forces

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/POSCAR_res_POSCAR_1.1.traj_forces
       :linenos:
       :language: text



Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_EAM_potential/reference_output/log
       :linenos:
       :language: text
